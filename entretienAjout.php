<?php
/**
 * Created by PhpStorm.
 * User: Célia
 * Date: 27/12/2016
 * Time: 17:39
 */

require 'app/bootstrap.php';

if (!Auth::logged() || !Auth::user()->estCandidat()) redirect('index.php');

$user = Auth::user();

$layout = new Layout('candidats');
include view('candidats/entretienAjout.php');
$layout->show('Ajout d\'un Entretien');