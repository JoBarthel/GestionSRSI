<?php
require 'app/bootstrap.php';

if (!Auth::logged() || !Auth::user()->estCandidat()) redirect('index.php');

$user = Auth::user();

$layout = new Layout('candidats');
include view('candidats/infoModif.php');
$layout->show('Informations');