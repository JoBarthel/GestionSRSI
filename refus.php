<?php

require 'app/bootstrap.php';

// Authentification requise
if (!Auth::logged()) redirect('index.php');

//etre un candidat requis
if (!Auth::user()->estCandidat()) error(403);

$offre = new OffreEmbauche($_GET['offre']);
$layout = new Layout('candidats');
include view('candidats/refus.php');
$layout->show('Raison du refus');
	

?>