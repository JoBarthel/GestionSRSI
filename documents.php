<?php

require 'app/bootstrap.php';

// Authentification requise
if(!Auth::logged()) redirect('index.php');


if(Auth::user()->estCandidat()) {

    $listeTypesDoc = TypeDoc::lister();
    $layout = new Layout('candidats');
    include view('candidats/documents.php');
    $layout->show('Mes Documents');
}
else
{

    $layout = new Layout('responsables');
    include view('responsables/documents.php');
    $layout->show('Documents Publics');
}

?>