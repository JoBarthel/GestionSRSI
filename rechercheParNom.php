<?php
require 'app/bootstrap.php';

//authentification requise
if (!Auth::logged()) redirect('index.php');

//être un responsable requis
if(!Auth::user()->estResponsable()) redirect('index.php');

$nom = Validation::chaine('nom', 25);
if($nom)$liste = Candidat::rechercheParNom(filter($nom));
else $liste = [];

//envoie vers la vue
$layout = new Layout('responsables');
include view('responsables/rechercheParNom.php');
$layout->show('Recherche');