<?php
/**
 * Created by PhpStorm.
 * User: Célia
 * Date: 28/12/2016
 * Time: 14:13
 */

require 'app/bootstrap.php';

//authentification requise
if (!Auth::logged()) redirect('index.php');

//être un responsable requis
if(!Auth::user()->estResponsable()) redirect('index.php');

//le candidat affiché par cette page
$candidat = new Candidat($_GET['numCandidat']);

//sa liste de docs
$listeDoc = Document::utilisateurDoc($candidat->numUtilisateur);

$listeOffresAcceptees = $candidat->toutesSesReponsesAcceptes();
$listeOffresRefusees = $candidat->toutesSesReponsesRefusees();
$listeRaisonsRefus = $candidat->toutesSesRaisonsRefus();

//envoie vers la vue
$layout = new Layout('responsables');
include view('responsables/afficherCandidat.php');
$layout->show('Informations de'.$candidat->prenom.' '.$candidat->nom);