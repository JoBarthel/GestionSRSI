<?php

require 'app/bootstrap.php';

// Authentification requise
if (!Auth::logged()) redirect('index.php');


if (Auth::user()->estResponsable()) {


    $listeStatuts = Statut::listerStatuts();
    $listeOffres = OffreEmbauche::listerOffres();

    $layout = new Layout('responsables');
    include view('responsables/rechercheCandidat.php');
    $layout->show('Recherche');

} else redirect('index.php');