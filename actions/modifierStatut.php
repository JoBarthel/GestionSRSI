<?php

require "../app/bootstrap.php";

//met à jour le statut du candidat
//ici on a une liste déroulante et un input caché donc pas d'input dangereux
$candidat = new Candidat($_POST['numCandidat']);
$candidat->setStatut($_POST['statut']);

//redirige vers la page du candidat
redirect('../afficherCandidat.php?numCandidat='.$candidat->numCandidat);