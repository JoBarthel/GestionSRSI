<?php

require '../app/bootstrap.php';

//ici on a des inputs cachés et une liste déroulante donc pas d'input dangereux
$candidat = new Candidat($_POST['numCandidat']);
$numRaison = $_POST['numRaison'];

//refuse la fiche de suivi de candidat pour la raison numéro $numRaison
$candidat->refuserFicheSuivi($numRaison);

//envoie le mail
notify($candidat, 'refus-suivi', $numRaison);

redirect('../fiches.php?numCandidat=' . $candidat->numCandidat);
