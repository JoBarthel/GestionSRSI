<?php
require '../app/bootstrap.php';

$responsable = new Responsable($_POST['numResponsable']);

//initialise les params de setAlerts
$alerteSurReset = 0;
$alerteSurNouveauCandidat = 0;

if ($_POST['alerteSurReset']) $alerteSurReset = 1;
if ($_POST['alerteSurNouveauCandidat']) $alerteSurNouveauCandidat = 1;


$responsable->setAlertes($alerteSurReset, $alerteSurNouveauCandidat);

flash("Vos alertes ont bien été mises à jour");

redirect('../configurerAlertesMail.php');
