<?php

require '../app/bootstrap.php';

//change le statut du document
//on a des inputs cachés et une liste déroulante donc pas d'input dangereux
$doc = new Document($_POST['numDoc']);
$doc->setStatut(StatutDoc::numStatutRefuseActuel());

//Change la raison du refus du document
$doc->setRaison($_POST['numRaison']);

//envoie un mail à l'utilisateur
$proprietaire = Utilisateur::get($doc->numUtilisateur);
notify($proprietaire, 'refus-doc', $doc->numDoc);

//redirige vers la page du candidat
redirect('../afficherCandidat.php?numCandidat='.$_POST['numCandidat']);