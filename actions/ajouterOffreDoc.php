<?php

require '../app/bootstrap.php';

//trouve le nom du fichier et vérifie s'il existe
$basename = basename($_FILES['uploadedfile']['name']);
if(!$basename)
{
    flash("Veuillez sélectionner un ficher");
    redirect('../ajouterOffre.php');
}

//trouve l'extension
$extension = pathinfo($basename, PATHINFO_EXTENSION);

//le nom du chemin
$target_path = __DIR__."/../documents/";
$emplacement = Auth::user()->nom.sha1( $basename.time());
$target_path = $target_path.$emplacement;

$target_path = $target_path.".".$extension;
echo $target_path;

if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
    echo "Le fichier ".basename($_FILES['uploadedfile']['name'])." a bien été mis en ligne.";

    //crée un nouvel objet document avec cet objet
    $d1 = Document::ajouteDocument(basename($_FILES['uploadedfile']['name']) , Auth::user()->numUtilisateur, $_POST["type"], $target_path, 0, $emplacement);

    //ajoute ce document à la liste des docs stockée en variables de session
    $listeDoc = $_SESSION['listeDoc'];
    $listeDoc[ count($listeDoc)] = $d1->numDoc;
    $_SESSION['listeDoc'] = $listeDoc;

    //redirige vers ajouterInfo.php
    redirect('../ajouterOffre.php');

}
else echo "Erreur avec la mise en ligne du fichier ";