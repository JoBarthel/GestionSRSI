<?php

require '../app/bootstrap.php';

Candidat::reinitialiserFichesSuivi();

Responsable::envoyerAlerte("alerteSurReset");

redirect('../reinitialiserFichesSuivi.php');