<?php
require "../app/bootstrap.php";

//pas de champs à remplir donc pas de validation nécessaire. on filtre quand même la valeur passée par GET
//change le statut du document
$doc = new Document(filter($_GET['numDoc']));
$doc->setStatut(StatutDoc::numStatutValideActuel());

//renvoie vers la page du candidat
redirect('../afficherCandidat.php?numCandidat='.$_GET['numCandidat']);