<?php
require '../app/bootstrap.php';
$numRelance = $_GET['numRelance'];

if(is_numeric($numRelance)) (new Relance(filter($numRelance)))->supprime();
else flash("Erreur : la relance n'a pas pu être supprimée");

redirect('../ficheEnt.php?offre='.$_GET['numOffre']);
