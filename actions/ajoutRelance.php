<?php

require '../app/bootstrap.php';

//pas de validation pour offre car input caché
$offre=$_POST['offre'];

$date=Validation::date('date');
if($date) flash("Date OK");

//pas de validation car input de type selecte
$type=$_POST['type'];

if($offre && $date && $type)
    Relance::ajoute(filter($date), $type);
else flash('Erreur : la relance n\'a pas pu être ajoutée');

redirect('ficheEnt.php?offre='.$offre.'');