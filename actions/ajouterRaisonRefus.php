<?php
require '../app/bootstrap.php';

//ajoute la raison
$nomRaison = Validation::chaine('nomRaison', 25);
$descRaison = Validation::chaine('descRaison', 100);

//filtre les valeurs
foreach (['nomRaison', 'descRaison'] as $item) {
    $$item = filter($$item);
}

//si tout ok ajoute la raison
if($nomRaison && $descRaison) {
    RaisonRefus::ajoute($_POST['nomRaison'], $_POST['descRaison']);
    flash('Votre raison a bien été ajoutéé');
}else flash('Erreur : la raison n\'a pas pu être ajoutéé' );


//redirige vers refuserDocument
redirect('../refuserDocument.php?numDoc='.$_POST['numDoc'].'&numCandidat='.$_POST['numCandidat']);