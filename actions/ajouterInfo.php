<?php
/**
 * Created by PhpStorm.
 * User: Célia
 * Date: 27/12/2016
 * Time: 23:16
 */
require '../app/bootstrap.php';

//valide le texte de l'info
if($info = Validation::chaine('texteInfo', 400))
    //ajoute l'info dans la base de données avec la date du jour et le texte envoyé via formulaire
    $info = InfoPublique::ajoute(date('Y-m-d'), filter($info));
else flash('Erreur : l\'information n\'a pas été enregistrée');

//Pour les documents dans la variable de session listeDoc, change leur appartenance à cette info
$listeDoc = $_SESSION['listeDoc'];
for($i = 0 ; $i < count($listeDoc) ; $i++)
{
    $doc = new Document($listeDoc[$i]);
    $doc->setInfo($info->numInfo);
}
//vide la listeDoc
$_SESSION['listeDoc'] = [];

//redirige
redirect('../infosPubliques.php');