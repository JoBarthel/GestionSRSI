<?php
require '../app/bootstrap.php';

$contents = file_get_contents($_FILES['uploadedfile']['tmp_name']);
$csv = new CSV($contents);

while($line = $csv->next()) {
    $prenom = filter($csv->get('prenom'));
    $nom = filter($csv->get('nom'));
    $mail = filter($csv->get('mail'));

    //vérifie les tailles
    $prenom = Validation::taille($prenom, 25);
    $nom = Validation::taille($nom, 25);
    $mail = Validation::taille($mail, 40);
    $mail = filter_var($mail, FILTER_VALIDATE_EMAIL);
    if($prenom && $nom && $mail)
    {
       $c = Candidat::ajouteCandidat($nom, $prenom, $mail);
       flash("Le candidat ".$c->nom." a bien été ajouté");
    }
    else echo("Erreur de lecture : l'un des candidats pourrait ne pas avoir été ajouté");
}


redirect('../ajout.php');
