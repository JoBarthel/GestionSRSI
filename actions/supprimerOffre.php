<?php

require ('../app/bootstrap.php');

//pas de champs à remplir donc pas de validation nécessaire. on filtre quand même la valeur passée par GET
$offre = new OffreEmbauche(filter($_GET['offre']));
$offre->supprimerOffre();

flash("L'offre a bien été suppriméé");

redirect('../offresEmbauche.php');