<?php
/**
 * Created by PhpStorm.
 * User: Célia
 * Date: 27/12/2016
 * Time: 16:38
 */
require '../app/bootstrap.php';

//on ne vérifie pas la date car si elle n'est pas valide ce sera la date par défaut
$date = Validation::date('date');

$resume = Validation::chaine('resume', 100);

//le numEntretien est un input caché donc pas de validation
$numEntretien = $_POST['entretien'];

//filtrel les valeurs
foreach (['date', 'resume', 'numEntretien'] as $item) $$item = filter($$item);

//si tout est ok alors ajouter l'entretien
if($date && $resume && $numEntretien) {
    $entretien = new Entretien($numEntretien);
    $entretien->updateEntretien($date, $resume);

    flash("L'entretien a bien été mis à jour");
    redirect("../ficheEnt.php?offre=".$entretien->numOffre);
}else
{
    flash("Erreur : l'entretien n'a pas pu être mis à jour");
    if(!$date) flash("Format de la date : AAAA-MM-JJ");
    redirect("../entretienModif.php?numEntretien=".$numEntretien);
}
