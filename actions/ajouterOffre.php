<?php

require '../app/bootstrap.php';

//valide les infos
$mail = Validation::email('mailRecruteur', 40);
$poste = Validation::chaine('poste', 40);
$societe = Validation::chaine('societe', 25);
$adresse = Validation::chaine('adresse', 50);
$nomRecruteur = Validation::chaine('nomRecruteur', 50);
$telRecruteur = Validation::nombre('telRecruteur', 10, 10);
$description = Validation::chaine('description', 200);
$instructions= Validation::chaine('instructions', 200);
foreach (['mail', 'poste', 'societe', 'adresse', 'nomRecruteur', 'telRecruteur', 'description', 'instructions'] as $item)
    $$item = filter($$item);

//si tout ok ajouter l'offre
if ($mail && $poste && $societe && $adresse && $nomRecruteur && $telRecruteur && $description && $instructions) {
    $offre = OffreEmbauche::ajouterOffre($poste, $societe, $adresse, $nomRecruteur,
        $mail, $telRecruteur, $description,$instructions, date('Y-m-d'));

    //fait la liste des candidats en recherche d'entreprise et la notiifie
    $listeCandidats = Candidat::candidatsEnRechercheEntreprise();
    foreach ($listeCandidats as $candidat) {
        if ($candidat->numStatut == Statut::numStatutRechercheEntreprise()) notify($candidat, 'publication-offre', $offre->numOffre);
    }

    //Pour les documents dans la variable de session listeDoc, change leur appartenance à cette offre
    $listeDoc = $_SESSION['listeDoc'];
    for($i = 0 ; $i < count($listeDoc) ; $i++)
    {
        $doc = new Document($listeDoc[$i]);
        $doc->setOffre($offre->numOffre);
    }

    //vide la listeDoc
    $_SESSION['listeDoc'] = [];

    flash("Votre offre a bien été ajoutée");
    redirect('../offresEmbauche.php');

} else {

    flash("Erreur : l'offre n'a pas pu être ajoutée");
    redirect('../ajouterOffre.php');
}

