<?php

require '../app/bootstrap.php';

///vérifie les valeurs
if (!$nom = Validation::chaine('nom', 25)) flash("Entrez un nom valide.");
if (!$prenom = Validation::chaine('prenom', 25)) flash("Entrez un prénom valide");
if (!$mail = Validation::email('mail', 40)) flash("Entrez un mail valide.");
$typeUser = ($_POST['typeUser'] == 'Candidat' || $_POST['typeUser'] == 'Responsable') ? $_POST['typeUser'] : false;

//retire les caractères spéciaux
foreach (['nom', 'prenom', 'mail'] as $item) {
    $$item = filter($$item);
}

//si tout ok
if ($nom && $prenom && $mail && $typeUser) {
//ajouter utilisateur
    if ($typeUser == 'Responsable') $nouvelUtilisateur = Responsable::ajouteResponsable($nom, $prenom, $mail);
    else if ($typeUser == 'Candidat') $nouvelUtilisateur = Candidat::ajouteCandidat($nom, $prenom, $mail);

    flash($typeUser . " " . $nom . " " . $prenom . " a été ajouté !");
}

if(!$typeUser) flash("Veuillez choisir candidat ou responsable");

redirect('../ajout.php');
