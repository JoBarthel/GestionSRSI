<?php
require "../app/bootstrap.php";

//pas de champs à remplir donc pas de validation nécessaire. on filtre quand même la valeur passée par GET
$candidat = new Candidat(filter($_GET['numCandidat']));

$candidat->supprimeCandidat();

redirect('../listeCandidats.php');