<?php

require '../app/bootstrap.php';

/*
 * Valide les données
 */

$mdp = Validation::chaine('mdp', 255);
$mdpConfirm =  Validation::chaine('mdpConfirm', 255);
$naissance = Validation::date('naissance');
$mail = Validation::email('mail', 40);
$tel = Validation::nombre('tel', 10, 10);
$diplome = Validation::chaine('diplome', 25);
$anneeBac =  Validation::chaine('anneeBac', 255);
$adresse =  Validation::chaine('adresse', 255);

$user = Auth::user();

if ($mdp != $mdpConfirm) {
    flash("Les mots de passe ne correspondent pas");
    redirect('../infoModif.php');

} else if($mdp && $mdpConfirm && $naissance && $mail && $tel && $diplome && $anneeBac && $adresse){
    /*
     * Filtre les données
     */
    foreach (['mdp', 'mdpConfirm', 'naissance', 'mail', 'tel', 'diplome', 'anneeBac', 'adresse'] as $item) {
        $$item = filter($$item);
    }

    //Modifie les données de l'utilisateur
    Utilisateur::get($user->numUtilisateur)->updateCandidat($mdp, $mail, $naissance, $tel, $diplome, $anneeBac, $adresse);

    redirect('../informations.php');
}
else
{
    flash("Erreur : les informations n'ont pas pu être modifiées");
    if(!$naissance) flash("Format de la date : AAAA-MM-JJ");
    redirect('../infoModif.php');
}


