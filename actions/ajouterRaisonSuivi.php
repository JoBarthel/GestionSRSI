<?php
require '../app/bootstrap.php';

//filtre et ajoute la valeur
if($nomRaison = Validation::chaine('nomRaison', 50))
    RaisonRefusSuivi::ajoute(filter($nomRaison));
else flash("Erreur : la raison n'a pas pu être ajoutéé");

redirect('../fiches.php?numCandidat='.$_POST['numCandidat']);
