<?php
require '../app/bootstrap.php';


$date = Validation::date('date');
if(!$date) flash("Format de date : AAAA-MM-JJ");

//pas besoin de validation ou de filtre car les inputs sont de type select et hidden
$type = $_POST['type'];
$numFicheSuivi = $_POST['numFicheSuivi'];

if($date && $type && $numFicheSuivi) {
    Relance::ajoute(filter($date), $type, $numFicheSuivi);
}else flash("Erreur : la relance n'a pas pu être ajoutée");

redirect('../ficheEnt.php?offre='.$_POST['numOffre']);


