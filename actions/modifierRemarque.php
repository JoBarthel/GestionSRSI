<?php

require('../app/bootstrap.php');

//pas de validation sur numFicheSuivi car c'est un input caché
$fiche = new FicheDeSuivi($_POST['numFicheSuivi']);
$redirect = '../ficheEnt.php?offre='.$fiche->numOffre;

if($texteRemarque = Validation::chaine('texteRemarque', 200))
{
    $fiche->setRemarque(filter($texteRemarque));
    redirect($redirect);
}
else
{
    (flash("Erreur : la remarque n'a pas pu être modifiée"));
    redirect($redirect.'&modifierRemarque=1');
}
