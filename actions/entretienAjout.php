<?php
/**
 * Created by PhpStorm.
 * User: Célia
 * Date: 27/12/2016
 * Time: 18:01
 */

require '../app/bootstrap.php';

$date = Validation::date('date');
$resume = Validation::chaine('resume', 100);

//le numOffre est un input caché donc pas de validation
$numOffre = $_POST["offre"];

if($date && $resume && $numOffre) {
    //filtre les inputs
    foreach (['date', 'resume'] as $item) $$item = filter($$item);
    Entretien::ajouteEntretien($date, $resume, $numOffre, Utilisateur::get(Auth::user()->numUtilisateur)->numCandidat);

    flash("L'entretien a bien été ajouté");
    redirect("../ficheEnt.php?offre=".$numOffre);
}else
{
    flash("Erreur : l'entretien n'a  pas pu être ajouté");
    if(!$date) flash("Format de la date : AAAA-MM-JJ");
    redirect("../entretienAjout.php?numOffre=".$numOffre);
}
