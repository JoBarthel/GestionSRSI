<?php

require '../app/bootstrap.php';

//ajoute le statut
if (($nomStatut = Validation::chaine('nomStatut', 25)) && ($descStatut = Validation::chaine('descStatut', 100))) {

    Statut::ajouteStatut(filter($nomStatut), filter($descStatut));
    flash("Votre statut a bien été ajouté");
}
else flash("Erreur : le statut n'a pas pu être ajouté");
//redirige
redirect("../modifierStatut.php?numCandidat=" . $_POST['numCandidat']);