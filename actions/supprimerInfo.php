<?php
require "../app/bootstrap.php";

//pas de champs à remplir donc pas de validation nécessaire. on filtre quand même la valeur passée par GET
$numInfo = filter($_GET['numInfo']);
$info = new InfoPublique($numInfo);

$info->supprime();

redirect('../infosPubliques.php');
