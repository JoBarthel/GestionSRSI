<?php

require "../app/bootstrap.php";

//change la validation du suivi dans la bdd
//pas de champs à remplir donc pas de validation nécessaire. on filtre quand même la valeur passée par GET
$candidat = new Candidat(filter($_GET['numCandidat']));
$candidat->validerFicheSuivi();

//redirige vers la page du candidat
redirect("../afficherCandidat.php?numCandidat=".$candidat->numCandidat);
