<?php

require "../app/bootstrap.php";

$mdp = Validation::chaine('mdp', 255);
$mdpConfirm = Validation::chaine('mdpConfirm', 255);

if($mdp != $mdpConfirm)
{
    flash("Les mots de passe ne correspondent pas");
    redirect('../changerMotDePasse.php');
}
else if ($mdp && $mdpConfirm)
{

    //Filtre les données
    foreach (['mdp', 'mdpConfirm'] as $item)
        $$item = filter($$item);

    //met à jour le mot de passe
    $user = Utilisateur::get($_POST['numUtilisateur']);
    $user->updateUtilisateur($mdp, $user->mail);

    //redirection
    flash("Le mot de passe a bien été mis à jour");
    redirect('../changerMotDePasse.php');

}else
{
    flash("Erreur : le mot de passe n'a pas pu être modifié");
    redirect('../changerMotDePasse.php');
}