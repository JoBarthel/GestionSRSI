<?php
/**
 * Created by PhpStorm.
 * User: Célia
 * Date: 27/12/2016
 * Time: 19:46
 */

require 'app/bootstrap.php';

// Authentification requise
if(!Auth::logged()) redirect('index.php');


$listeInfos = InfoPublique::listerInfos();

//si candidat, alors on peut voir les infos publiques
if(Auth::user()->estCandidat()) {



    $layout = new Layout('candidats');
    include view('candidats/infosPubliques.php');
    $layout->show('Informations');
}

//si responsable, on peut voir les infos publiques et en ajouter
if(Auth::user()->estResponsable()) {

    $layout = new Layout('responsables');
    include view('responsables/infosPubliques.php');
    $layout->show('Informations');

}

