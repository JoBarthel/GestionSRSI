<?php

require('app/bootstrap.php');

// Authentification requise
if (!Auth::logged()) redirect('index.php');

//être un responsable requis
if (!Auth::user()->estResponsable()) {
    redirect('index.php');
}


$layout = new Layout('responsables');
include view('responsables/reinitialiserFichesSuivi.php');
$layout->show('Réinitialiser la validation des fiches de suivi');