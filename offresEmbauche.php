<?php

require 'app/bootstrap.php';

//Authentification requise
if (!Auth::logged()) redirect('index.php');

//si responsable on peut voir les offres d'embauche
if(Auth::user()->estResponsable())
{

    //liste des offres d'embauche
    $listeOffres = OffreEmbauche::listerOffres();

    $layout = new Layout('responsables');
    include view('responsables/offresEmbauche.php');
    $layout->show('Gestion des offres d\'embauches');

}