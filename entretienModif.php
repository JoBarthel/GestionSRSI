<?php
require 'app/bootstrap.php';

if (!Auth::logged() || !Auth::user()->estCandidat()) redirect('index.php');

$user = Auth::user();
$numEntretien = $_GET['numEntretien'];
$entretien = new Entretien($numEntretien);

$layout = new Layout('candidats');
include view('candidats/entretienModif.php');
$layout->show('Entretiens');