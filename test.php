<?php

try {

    require 'app/bootstrap.php';

    $layout = new Layout('candidats');

    if(Auth::logged()) {
        echo '<h3>Utilisateur actuel</h3><pre>'.print_r(Auth::user(), true).'</pre>';
    }

    echo '<h3>Contenu de $_SESSION</h3><pre>'.print_r($_SESSION, true).'</pre>';

    echo '<h3>Exemples de requètes (voir code source)</h3>';
    echo '<pre>';

    /**
     * Récupérer une ligne avec des paramètres
     */

    $query = db()->prepare('SELECT * FROM utilisateur WHERE prenom = ? AND nom = ?');
    $query->execute(['Jean', 'Candidat']);

    if($query->rowCount() != 0) {
        $candidat = $query->fetch();
        print_r($candidat);
    } else {
        echo 'Aucun candidat trouvé pour ces noms et prénoms.'.PHP_EOL;
    }

    /**
     * Récupérer une ligne sans paramètres
     */

    $query = db()->query('SELECT * FROM candidat ORDER BY numCandidat ASC');

    if($query->rowCount() != 0) {
        $candidat = $query->fetch(); // Premier résultat
        print_r($candidat);
    } else {
        echo 'Aucun candidat trouvé.'.PHP_EOL;
    }

    /**
     * Pour tous les enregistrements...
     */

    $query = db()->query('SELECT * FROM candidat ORDER BY numCandidat ASC');

    if($query->rowCount() != 0) {
        foreach($query->fetchAll() as $candidat) {
            print_r($candidat);
        }
    } else {
        echo 'Aucun candidat trouvé.'.PHP_EOL;
    }

    /**
     * Modifier un enregistrement
     */

    $query = db()->prepare('UPDATE candidat SET prenom = ? WHERE nom = ?');
    //$query->execute(['Jean', 'Candidat']}); (on ne souhaite pas éxecuter la requète sur la page de test)

    /**
     * Supprimer un enregistrement
     */

    //db()->prepare('DELETE FROM candidat WHERE prenom = ?')->execute(['Jean']);

    /**
     * Insérer un enregistrement
     */

    //db()->prepare('INSERT INTO candidat SET prenom = ?, nom = ?')->execute(['Jean', 'Dupont']);


    /**
     * Tests d'affichages multiples (récupère toutes les lignes à la fois)
     */

    // Affiche un tableau PHP contenant tous les utilisateurs
    print_r(db()->query('SELECT * FROM utilisateur')->fetchAll());


    // Affiche un tableau PHP contenant tous les responsables
    print_r(db()->query('SELECT * FROM responsable')->fetchAll());

    // Affiche un tableau PHP contenant les utilisateurs étant responsables
    print_r(db()->query('SELECT * FROM utilisateur WHERE numResponsable IS NOT NULL')->fetchAll());

    // Affiche un tableau PHP contenant les utilisateurs étant responsables (+ leurs informations de candidat)
    print_r(db()->query('SELECT * FROM utilisateur u, responsable r WHERE u.numResponsable IS NOT NULL AND u.numResponsable = r.numResponsable')->fetchAll());


    // Affiche un tableau PHP contenant tous les candidats
    print_r(db()->query('SELECT * FROM candidat')->fetchAll());

    // Affiche un tableau PHP contenant les utilisateurs étant candidats
    print_r(db()->query('SELECT * FROM utilisateur WHERE numCandidat IS NOT NULL')->fetchAll());

    // Affiche un tableau PHP contenant les utilisateurs étant candidats (+ leurs informations de candidat)
    print_r(db()->query('SELECT * FROM utilisateur u, candidat c WHERE u.numCandidat IS NOT NULL AND u.numCandidat = c.numCandidat')->fetchAll());

    echo '</pre>';

    $layout->show('Tests');

}
catch(Exception $e) {
    print_r($e);
    debug_print_backtrace();
}