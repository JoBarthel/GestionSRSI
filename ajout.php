<?php
	require 'app/bootstrap.php';
	
	//Authentification requise
	if(!Auth::logged()) redirect('index.php');

	//Etre un responsable requis
	if(Auth::user()->estResponsable()) {

			$layout = new Layout('responsables');
			include view('responsables/ajout.php');
			$layout->show('Page d\'Ajout');

	}else error(403);
?>