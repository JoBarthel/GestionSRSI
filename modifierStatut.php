<?php

require 'app/bootstrap.php';

//authentification requise
if (!Auth::logged()) redirect('index.php');

//être un responsable requis
if(!Auth::user()->estResponsable()) redirect('index.php');

//le candidat dont le statut est modifié
$candidat = new Candidat($_GET['numCandidat']);

//La liste des statuts dispo
$listeStatut = Statut::listerStatuts();

//envoie vers la vue
$layout = new Layout('responsables');
include view('responsables/modifierStatut.php');
$layout->show("Modifier le statut de ".$candidat->getNomComplet());