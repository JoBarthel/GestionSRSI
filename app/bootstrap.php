<?php

session_start();

class Database {
    protected static $db = null;
    public static function get() {
        if(is_null(self::$db)) {
            try {
                $asus = gethostname() == 'Asuzorus';
                self::$db = new PDO("mysql:host=localhost;dbname=".($asus ? 'projetjcf' : 'projet').";charset=utf8", "root", $asus ? 'root' : '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                self::$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            }
            catch (Exception $e) {
                error(500, "Erreur de connexion à la BDD : " . $e->getMessage());
            }
        }
        return self::$db;
    }
}

function db() { return call_user_func_array(array('Database', 'get'), func_get_args()); }

function __autoload($classname) {
	$dirs = ['models', 'class'];
	foreach($dirs as $dir) {
		$dir = __DIR__.'/'.$dir;
		if(file_exists($dir.'/'.$classname.'.php')) {
			require $dir.'/'.$classname.'.php';
			break;
		}
	}
	return class_exists($classname);
}

/**
 * Affiche une page d'erreur
 * @param $code
 */
function error($code = 500, $message = null) {
    if(is_null($message)) {
        switch($code) {
            case 500:
                $message = 'Une erreur interne est survenue !';
                break;
            case 404:
                $message = 'Cette page est introuvable !';
                break;
            case 403:
                $message = 'Acces refuse !';
                break;
            default;
                $message = 'Une erreur est survenue !';
        }
    }
	include 'views/_error.php';
	die;
}

/**
 * Retourne le chemin vers la vues données en argument
 * @param $file
 * @return string
 */
function view($file) {
    $path = __DIR__.'/views/'.$file;
    if(!file_exists($path)) {
        error(500, "La vue ".$file." n'existe pas.");
    }
	return $path;
}

/**
 * Redirige vers la page passée en paramètre
 * @param $file
 */
function redirect($file) {
	header('Location: '.$file);
	die;
}

/**
 * Vérifie si TOUS les noms de champs passés en paramètres ont été postés et remplis
 * ex : sent('username', 'password') pour vérifier l'existence de $_POST['username'] et $_POST['password']
 * @return bool
 */
function sent() {
    $fields = func_get_args();
    foreach($fields as $f) {
        if(empty(input($f))) return false;
    }
    return true;
}

/**
 * Si $message est passé en paramètre, un message éphémère est stocké dans la session, pour être affiché sur la page suivante
 * Sinon, retourne un tableau contenant tous les messages éphémères en attente d'affichage
 * @param null $message
 * @return array
 */
function flash($message = null) {
    if(!isset($_SESSION['flash']) || !is_array($_SESSION['flash'])) $_SESSION['flash'] = [];
    if(is_null($message)) {
        $messages = $_SESSION['flash'];
        $_SESSION['flash'] = [];
        return $messages;
    }
    $_SESSION['flash'][] = $message;
}

/**
 * Retourne la valeur du champ $_POST[$input] (ou une chaine vide)
 * @param $input
 * @return string
 */
function input($input, $default = '') {
    return !empty($_POST[$input]) && trim($_POST[$input]) != '' ? $_POST[$input] : $default;
}

/**
*Retire les accents de la chaîne en entrée
*@param string $chaine, $charset
*@return string
*/
function enleveAccent($str, $charset = 'utf-8')
{
    $str = htmlentities($str, ENT_NOQUOTES, $charset);

    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

    return $str;
}

/**
 * Filtre les caractères HTML
 */
function filter($str){
    return htmlspecialchars($str);
}

/*
 * Envoie un mail du type concerné à l'utilisateur $user
 * notify($user, $mail_type, [$doc, $offreEmbauche, $numRaison, $motDePasse]);
 */
function notify()
{
    $arguments = func_get_args();
    $user = $arguments[0];
    $mail_type = $arguments[1];

    ob_start();
    $sujet = "Notification automatique recrutement Licence SRSI";
    if($mail_type == 'confirmation-inscription')
    {
        $sujet = "Recrutement Licence SRSI - confirmation d'inscription";
        $login = $user->login;
        //$mdp = $user->motDePasse;

        //le 3eme paramètre est le mot de passe
        $mdp = $arguments[2];
        include view('mails/vueConfirmationInscription.php');
    }
    else if($mail_type == 'refus-doc')
    {
        $sujet = "Recrutement Licence SRSI - document refusé";

        //le 3eme paramètre contient le numéro de doc
        $doc = new Document($arguments[2]);
        $raisonRefus = new RaisonRefus($doc->numRaison);

        include view('mails/vueRefusDocument.php');
    }
    else if($mail_type == 'publication-offre')
    {

        $sujet = "Recrutement Licence SRSI - nouvelle offre d'embauche";

        //le 3eme paramètre contient le numéro de l'offre
        $offre = new OffreEmbauche($arguments[2]);

        include view('mails/vuePublicationOffre.php');
    }
    else if($mail_type == 'refus-suivi')
    {

        $sujet = "Recrutement Licence SRSI - refus de votre fiche de suivi";

        //le 3eme paramètre contient le numéro de la raison
        $raison = new RaisonRefusSuivi($arguments[2]);
        include view('mails/vueRefusSuivi.php');
    }
    else if($mail_type=="alerteSurReset")
    {
        $sujet = "Recrutement Licence SRSI - réinitialisations de la validation des fiches de suivis";

        include view('mails/vueReinitFicheSuivi.php');
    }
    else if($mail_type=="alerteSurNouveauCandidat")
    {
        $sujet = "Recrutement Licence SRSI - Nouveau candidat";

        include view('mails/vueNouveauCandidat.php');
    }
    $contents = ob_get_contents();
    ob_end_clean();
   

    if(!mail($user->mail, $sujet, $contents)) flash("L'envoi du mail a échoué");
}

/**
 * Generate a random string, using a cryptographically secure
 * pseudorandom number generator (random_int)
 *
 *
 * @param int $length      How many characters do we want?
 * @param string $keyspace A string of all possible characters
 *                         to select from
 * @return string
 */
function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[rand(0, $max)];
    }
    return $str;
}

Auth::init();