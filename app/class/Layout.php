<?php

class Layout {

    /**
     * Layouts
     */

    public function pd($layout, $titre = 'SRSI') {
        include $layout;
    }

    // Layout : _layouts/candidats.php (on peut passer un titre à la fonction show() */
    public function candidats($layout, $title = '') {
        $utilisateur = Auth::user();
        include $layout;
    }

    // Layout : _layouts/visiteurs.php */
    public function visiteurs($layout, $title = 'Licence SRSI', $bigtitle = 'Bienvenue !') {
        include $layout;
    }


    /**
     * Ne pas toucher
     */

    private $layout;
    private $contents;

    public function __construct($layout) {
        $this->layout = $layout;
        ob_start();
    }

    public function show() {
        $this->contents = ob_get_contents();
        ob_end_clean();
        $path = view('_layouts/'.$this->layout.'.php');
        if(method_exists($this, $this->layout)) call_user_func_array([$this, $this->layout], array_merge([$path], func_get_args()));
        else include $path;

    }

    public function contents() {
        return $this->contents;
    }
}