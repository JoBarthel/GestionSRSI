<?php

class Validation
{
    /**
     * Usage pour vérifier la taille de $valeur :
     * $valeur = Validation::taille($valeur, 500, 255);
     * ($valeur === false si taille invalide)
     * @param $valeur
     * @param null $tailleMax
     * @param int $tailleMin
     * @return $valeur ou bool
     */
    public static function taille($valeur, $tailleMax = null, $tailleMin = 0)
    {
        return strlen($valeur) < $tailleMin || (!is_null($tailleMax) && strlen($valeur) > $tailleMax) ? false : $valeur;
    }

    /**
     * Usage pour récupérer et valider $_POST['adressemail'] :
     *     $email = Validation::email('adressemail');
     *
     * Usage pour récupérer et valider $_POST['adressemail'] (max: 500 caractères) :
     *     $email = Validation::email('adressemail', 500);
     *
     * Usage pour récupérer et valider $_POST['adressemail'] (max : 500 caractères, min : 12) :
     *     $email = Validation::email('adressemail', 500, 12);
     *
     * Exemple de code suivant :
     *
     * if($email !== false) {
     *    echo $email;
     * }
     * else {
     *    echo 'Adresse invalide.';
     * }
     *
     * @return $_POST['adressemail'] ou false
     */
    public static function email()
    {
        $filtre = FILTER_VALIDATE_EMAIL;

        $args = func_get_args();
        $args[0] = input($args[0]);
        return filter_var($args[0], $filtre) ? call_user_func_array([Validation::class, "taille"], $args) : false;
    }

    /**
     * Même usage que email() pour les nombres (eventuellement flottants)
     */
    public static function nombre()
    {
        $filtre = FILTER_VALIDATE_FLOAT;

        $args = func_get_args();
        $args[0] = input($args[0]);
        return filter_var($args[0], $filtre) ? call_user_func_array([Validation::class, "taille"], $args) : false;
    }

    /**
     * Même usage que email() pour les strings
     */
    public static function chaine()
    {
        $args = func_get_args();
        $args[0] = input($args[0]);
        return is_string($args[0]) ? call_user_func_array([Validation::class, "taille"], $args) : false;
    }

    /*
     *Usage pour récupérer et valider $_POST['date']
     *$date = Validation::date('date', $format='Y-m-d');
     */
    public static function date()
    {
        $args = func_get_args();
        $date = input($args[0]);
        if(isset($args[1])) $format = $args[1];
        else $format='Y-m-d';

        $checkDate = strtotime($date);
        return ($checkDate && (date($format, $checkDate) === $date)) ? $date : false;
    }

}