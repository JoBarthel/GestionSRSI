<?php

class Auth
{
    private static $user = false;

    // Cette méthode est appelée automatique au démarrage de l'application
    public static function init() {
        if(!empty($_SESSION['user'])) {
            try {
                self::$user = Utilisateur::get($_SESSION['user']);
            }
            catch(Exception $e) {
                self::logout();
                error(500);
            }
        }
    }

    // Retourne true si l'utilisateur est connecté
	public static function logged() {
	    return self::$user !== false;
    }

    // Retourne l'objet Utilisateur de l'utilisateur courant
    public static function user() {
	    return self::$user;
    }

    // Créé la session utilisatuer
    public static function login($id) {
        $_SESSION['user'] = $id;
    }

    // Détruit la session
    public static function logout() {
	    unset($_SESSION['user']);
    }
}