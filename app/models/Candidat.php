<?php

/*Informations personnelles d'un utilisateur de type candidate */

//TO DO : pouvoir ajouter un statut
class Candidat extends Utilisateur
{

    //infos persos
    public $numCandidat;
    public $dateNaissance;
    public $adresse;
    public $telephone;
    public $numStatut;

    //infos cursus

    public $anneeBac;
    public $diplomeBacPlus2;

    //infos parcours

    public $entrepriseActuelle;

    public $statutFicheSuivi;
    public $numRaison;

    public function __construct($numCandidat)
    {
        $query = db()->prepare('SELECT * FROM candidat WHERE numCandidat = ?');
        $query->execute([$numCandidat]);

        if ($query->rowCount() != 1) throw new Exception("Candidat introuvable #" . $numCandidat);

        $candidat = $query->fetch();

        // On appelle le constructeur de Utilisateur
        parent::__construct($candidat->numUtilisateur, 'candidat');

        // On injecte chaque colonne de la table Candidat dans un attribut
        foreach (['numCandidat', 'numStatut', 'dateNaissance', 'telephone', 'adresse', 'anneeBac', 'diplomeBacPlus2',  'entrepriseActuelle', 'statutFicheSuivi', 'numRaison'] as $attr) {
            $this->$attr = $candidat->$attr;
        }
    }

    public function rechercheEntreprise() {
        return $this->numStatut == 10;
    }

    /*Ici on peut place les méthodes pour les utilisateurs de type Candidat*/

    //affiche tout les données de la table candidat correspondant à son numéro
    public function afficher()
    {
        $query = db()->prepare('SELECT * FROM candidat WHERE numCandidat = ?');
        $query->execute([$this->numCandidat]);
        return $query->fetch();
    }

    //met à jour les données de ce candidat dans la bdd (et dans cet objet aussi)
    public function updateCandidat($motDePasse, $mail, $dateNaissance, $telephone, $diplomeBacPlus2, $anneeBac, $adresse)
    {

        $dateNaissance = date('Y-m-d', strtotime($dateNaissance));
        $this->dateNaissance = $dateNaissance;
        $this->telephone = $telephone;
        $this->diplomeBacPlus2 = $diplomeBacPlus2;
        $this->anneeBac = $anneeBac;
        $this->adresse = $adresse;

        //update l'utilisateur correspondant
        parent::updateUtilisateur($motDePasse, $mail);

        //met à jour la table utilisateur correspondante
        $query = db()->prepare("UPDATE candidat SET dateNaissance = ?, telephone = ?, diplomeBacPlus2 = ?, adresse = ?, anneeBac = ? WHERE numCandidat = ?");
        $query->execute([$dateNaissance, $telephone, $diplomeBacPlus2,$adresse, $anneeBac, $this->numCandidat]);


    }

    //Supprime ce candidat dans la BDD (et l'utilisateur correspondant)
    public function supprimeCandidat()
    {

        $query = db()->prepare("DELETE FROM candidat WHERE numCandidat = ?");
        $query->execute([$this->numCandidat]) or die("Le candidat " . $this->numCandidat . " n'a pas supprimé correctement.");

        $query = db()->prepare("DELETE FROM utilisateur WHERE numUtilisateur = ?");
        $query->execute([$this->numUtilisateur]) or die("L utilisateur " . $this->numCandidat . " n'a pas supprimé correctement.");




    }

    //modifie le statut de ce candidat
    public function setStatut($numStatut)
    {
        $query = db()->prepare('UPDATE candidat SET numStatut = ? WHERE numCandidat = ?');
        $query->execute([$numStatut, $this->numCandidat]);

        $this->numStatut = $numStatut;
    }

    //recupere le statut du candidat
    public function getStatut()
    {
        return new Statut($this->numStatut);
    }

    //retourne vrai si le candidat n'a pas d'entreprise, faux sinon
    public function verificationRecherche()
    {
        return empty($this->entrepriseActuelle);
    }

    //renvoie toutes les  réponses positives à des offres de ce candidat dans un tableau
    public function toutesSesReponsesAcceptes()
    {
        $query = db()->prepare("SELECT * FROM reponseoffre WHERE numCandidat = ? AND accepte = ?");
        $query->execute([$this->numCandidat, 1]);

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $array[$counter] = new OffreEmbauche($row['numOffre']);
            $counter++;
        }
        return $array;
    }

    //renvoie toutes les réponses négatives
    public function toutesSesReponsesRefusees()
    {
        $query = db()->prepare("SELECT numOffre FROM reponseoffre WHERE numCandidat = ? AND accepte = ?");
        $query->execute([$this->numCandidat, 0]);

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $array[$counter] = new OffreEmbauche($row['numOffre']);
            $counter++;
        }
        return $array;
    }

    //renvoie toutes les raisons des réponses négatives
    public function toutesSesRaisonsRefus()
    {
        $query = db()->prepare("SELECT motifRefus FROM reponseoffre WHERE numCandidat = ? AND accepte = ?");
        $query->execute([$this->numCandidat, 0]);

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $array[$counter] = $row['motifRefus'];
            $counter++;
        }
        return $array;
    }

    //accepte la fiche de suivi de ce candidat
    public function validerFicheSuivi()
    {
        $query = db()->prepare("UPDATE candidat SET statutFicheSuivi= ? WHERE numCandidat = ?");
        $query->execute([Candidat::numSuiviValide(), $this->numCandidat]);
    }
    //refuse la fiche de suivi de ce candidat pour la raison $numRaison
    public function refuserFicheSuivi($numRaison)
    {
        $query = db()->prepare("UPDATE candidat SET statutFicheSuivi = ?, numRaison = ? WHERE numCandidat = ?");
        $query->execute([Candidat::numSuiviRefuse(), $numRaison, $this->numCandidat]);
    }

    //renvoie vrai si le candidat a répondu à cette offre
    public function aRepondu($numOffre)
    {
        $query = db()->prepare("SELECT * FROM reponseoffre WHERE numCandidat = ? AND numOffre = ?");
        $query->execute([$this->numCandidat, $numOffre]);

        return ($query->rowCount() > 0);
    }

    //renvoie l'historique du candidat
    public function sonHistorique()
    {
        $query = db()->prepare("SELECT dateValidation, numStatut FROM historiqueCandidat WHERE numCandidat = ? ORDER BY  dateValidation DESC ");
        $query->execute([$this->numCandidat]);

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

            $array[$counter][0] = $row['dateValidation'];
            $array[$counter][1] = $row['numStatut'];
            $counter++;
        }
        return $array;


    }

    //enregistre un historique avec le statut actuel et la date du jour
    public function enregistrerHistorique()
    {
        $query = db()->prepare("INSERT INTO historiqueCandidat (numCandidat, dateValidation, numStatut) VALUES (?, ?, ?)");
        $query->execute([$this->numCandidat, date('Y-m-d'), $this->statutFicheSuivi]);
    }

    /* Méthodes statiques */
    //renvoie la liste des candidats sous forme d'un résultat
    /* Usage :
    $listeCandidat = Candidat::listerCandidats();
    while($row = $listeCandudat->fetch(PDO::FETCH_ASSOC)) echo $row["nom"]." ".$row["prenom"]."</br>";
    */
    /**
     * @return mixed
     */
    public static function listerCandidats()
    {
        $query = db()->prepare('SELECT numCandidat FROM candidat');
        $query->execute();

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

            $array[$counter] = $row['numCandidat'];
            $counter++;
        }
        return $array;
    }

    //renvoie le candidat correspondant à ce numéro
    public static function trouverCandidat($numCandidat)
    {
        return new Candidat($numCandidat);

    }

    //ajoute le candidat correspondant à ces informations dans la base de données et retourne l'objet avec ces données
    //ou faux si l'ajout a échoué
    public static function ajouteCandidat($nom, $prenom, $mail)
    {
        //génère un mot de passe de 10 chiffres aléatoire
        $nouveauMotDePasse = Utilisateur::genererMotDePasse();

        //crée l'utilisateur correspondant et stocke son ID dans utilID
        $utilId = parent::ajouteUtilisateur($nom, $prenom, $mail, $nouveauMotDePasse);

        //ajoute le candidat dans la BDD avec le numéro d'utilisateur correspondant et les valeurs par défaut adéquate
        $myDB = db();
        $query = $myDB->prepare("INSERT INTO candidat (numUtilisateur, numStatut, statutFicheSuivi) VALUES (?, ?, ?)");
        $query->execute([$utilId, Statut::numStatutDefautActuel(), Candidat::numSuiviEnAttente()]);

        //récupère le numCandidat et l insère dans la table utilisateur, à la ligne correspondante à ce candidat
        $candidatID = $myDB->lastInsertId();
        $query = $myDB->prepare("UPDATE utilisateur SET numCandidat = ? WHERE numUtilisateur = ?");
        $query->execute([$candidatID, $utilId]);

        //crée un objet avec les infos du candidat
        $candidat = new Candidat($candidatID);

        //envoie un mail au candidat avec son mot de passe non hashe
        notify($candidat, 'confirmation-inscription', $nouveauMotDePasse);

        //affiche les identifiants du compte à l'écran
        flash("Login :".$candidat->login." Mot de passe : ".$nouveauMotDePasse);

        //envoie un mail aux responsables qui le souhaitent
        Responsable::envoyerAlerte("alerteSurNouveauCandidat");

        //retourne ce candidat
        return $candidat;
    }

    //renvoie la liste des candidats portant le nom ou le prénom passé en paramètre
    public static function rechercheParNom($nom)
    {
        $query = db()->prepare("SELECT numCandidat FROM candidat NATURAL JOIN utilisateur WHERE nom LIKE ? OR prenom LIKE ?");
        $query->execute([$nom, $nom]);

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new Candidat($row['numCandidat']);
            $counter++;
        }
        return $array;
    }

    //renvoie la liste des candidats ayant le statut passé en paramètres
    public static function rechercheParStatut($numStatut)
    {
        $query = db()->prepare("SELECT numCandidat FROM candidat WHERE numStatut = ?");
        $query->execute([$numStatut]);

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new Candidat($row['numCandidat']);
            $counter++;
        }
        return $array;
    }

    //renvoie la liste des candidats ayant répondu favorablement à l'offre passée en paramètres
    public static function rechercheParOffre($numOffre)
    {
        $query = db()->prepare("SELECT numCandidat FROM reponseoffre WHERE numoffre = ? AND accepte = ?");
        $query->execute([$numOffre, 1]);

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new Candidat($row['numCandidat']);
            $counter++;
        }
        return $array;
    }

    //renvoie un tableau avec tout les candidats en recherche d'entreprise
    public static function candidatsEnRechercheEntreprise()
    {
        $query = db()->prepare("SELECT numCandidat FROM candidat WHERE numStatut = ?");
        $query->execute([Statut::numStatutRechercheEntreprise()]);

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new Candidat($row['numCandidat']);
            $counter++;
        }
        return $array;
    }

    //renvoie la valeur du statut qui correspond à fiche de suivi valide
    public static function numSuiviValide() { return 2;}

    //renvoie la valeur du statut qui correspond à une fiche de suivi refusée
    public static  function numSuiviRefuse() { return 3;}

    //renvoie la valeur du statut qui correpond à une fiche de suivi en attente
    public static function numSuiviEnAttente(){ return 1;}

    //réinitialise le statutFicheSuivi de tout les candidats de la bdd
    public static function reinitialiserFichesSuivi()
    {
        //enregistre l'historique de chaque candidat
        $liste = Candidat::listerCandidats();
        foreach ($liste as $c)
        {
            $candidat = new Candidat($c);
            $candidat->enregistrerHistorique();
        }

        //reinitialise les statuts de toutes les fiche de suivi.
        $query = db()->prepare("UPDATE candidat SET statutFicheSuivi = ?, numRaison = ? ");
        $query->execute([Candidat::numSuiviEnAttente(), null]);

        $query = db()->prepare("UPDATE datereinitialisation SET dateProchaineReinitialisation = ?");

        //calcule la date du jour + 1 semaine
        $date = date('Y-m-d', strtotime('+1 week'));
        $query->execute([$date]);

        flash("Les fiches de suivis des candidats sont de nouveau en attente de validation");
    }



    //affiche la date de la prochaine réinitialisation
    public static function prochaineInitialisation()
    {
        $query = db()->prepare("SELECT dateProchaineReinitialisation FROM datereinitialisation");
        $query->execute();

        $result = $query->fetch(PDO::FETCH_COLUMN);
        return $result;
    }

}

