<?php

class Statut {	

	public $numStatut;
	public $nomStatut;
	public $descStatut;

	//cherche les infos dans la base de données qui correspondent à ce numéro et met les infos à jour
	//attention rentrer un numéro qui n'existe pas dans la db crée une fatal error
	public function __construct($numStatut)
	{
		$this->numStatut = $numStatut;

		$query = db()->prepare('SELECT * FROM statut WHERE numStatut = ?');
		$query->execute([$numStatut]);

		if($query->rowCount() != 1) throw new Exception("Statut introuvable #".$numStatut);

		$statut = $query->fetch();
		
		//injecte chaque colonne de Statut dans les attributs
		foreach (['numStatut', 'nomStatut', 'descStatut'] as $attr) {
					$this->$attr = $statut->$attr;
				}		

	}


	//met ce statut à jour avec les infos passées en paramètre et met la BDD à jour
	public function updateStatut($nomStatut, $descStatut){

		$this->nomStatut = $nomStatut;
		$this->descStatut = $descStatut;

		$query = db()->prepare("UPDATE statut SET nomStatut = ?, descStatut = ? WHERE numStatut = ?");
		$query->execute([$nomStatut, $descStatut, $this->numStatut]) or die("Le statut ".$this->numStatut." n'a pas été mis à jour correctement.");

	}
	//Supprime ce statut dans la BDD
	public function supprimeStatut() {

		$query = db()->prepare("DELETE FROM statut WHERE numStatut = ?");
		$query->execute([$this->numStatut]) or die("Le statut ".$this->numStatut." n'a pas supprimé correctement.") ;

	}

	/*

	PARTIE STATIQUE 

	*/

	//retourne la liste des statuts en un PDOStatement
	/* Exemple pour récupérer les résultats 
	
	$listeStatut = ControleStatut::listerStatuts();

	while($row = $listeStatut->fetch(PDO::FETCH_ASSOC))
	{
		echo $row["numStatut"].' : '.$row["nomStatut"].' '.$row["descStatut"].'</br>';
	}
	*/
	public static function listerStatuts()
	{
		$query = db()->prepare('SELECT numStatut FROM statut');
		$query->execute();

		$array=[];
		$counter = 0;

		while($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = $row['numStatut'];
            $counter++;
        }

        return $array;


	}

	//retourne le statut correspondant a $numStatut
	public static function trouverStatut($numStatut)
	{
		return new Statut($numStatut);
	}

    //retourne le statut par défaut actuel des candidats
    public static function numStatutDefautActuel()
    {
        return 10;
    }

    //retourne le numéro de statut actuel équivalent  à "En recherche d'entreprise"
    public static function numStatutRechercheEntreprise()
    {
        return 10;
    }

	//crée le statut dans la BD correspondant à ces infos et retourne un objet Statut contenant ces données
	public static function ajouteStatut($nomStatut, $descStatut)
	{	
		$myDB = db();

		$query = $myDB->prepare('INSERT INTO statut (`nomStatut`, `descStatut`) VALUES (?, ?)');
		$query->execute([$nomStatut, $descStatut]) or die("Erreur lors de l'insertion du statut #".$nomStatut);

		$lastID = $myDB->lastInsertId();
		return new Statut($lastID);
	}

	
}