<?php

/**
 * Created by PhpStorm.
 * User: Joséphine
 * Date: 24/12/2016
 * Time: 23:15
 */
class ReponseOffre
{
    public $numCandidat, $numOffre, $accepte, $motifRefus, $numReponseOffre;

    public function __construct()
    {
        if(count(func_get_args()) == 2) {
            list($numCandidat, $numOffre) = func_get_args();
            $query = db()->prepare("SELECT * FROM reponseoffre WHERE numCandidat = ? AND numOffre = ?");
            $query->execute([$numCandidat, $numOffre]);
            if($query->rowCount() != 1) throw new Exception("Erreur : réponse à l'offre #".$numOffre." du candidat #".$numCandidat." introuvable.");
        }
        else {
            $numReponseOffre = func_get_args()[0];
            $query = db()->prepare("SELECT * FROM reponseoffre WHERE numReponseOffre = ?");
            $query->execute([$numReponseOffre]);
            if($query->rowCount() != 1) throw new Exception("Erreur : réponse #".$numReponseOffre." introuvable.");
        }

        $reponseOffre = $query->fetch();

        foreach (['numCandidat', 'numOffre', 'accepte', 'motifRefus', 'numReponseOffre'] as $attr)
            $this->$attr = $reponseOffre->$attr;
    }

    //supprime cette offre
    public function supprimeReponseOffre()
    {
        $query = db()->prepare("DELETE FROM reponseoffre WHERE numOffre = ? AND numCandidat = ?");
        $query->execute([$this->numOffre, $this->numCandidat]);
    }

    //Affiche tous les entretiens qui correspondent à cete réponse
    public function  toutSesEntretiens()
    {
        $query = db()->prepare("SELECT numEntretien FROM entretien WHERE numOffre = ? AND numCandidat = ?");
        $query->execute([$this->numOffre, $this->numCandidat]);

        $array = [];
        $counter = 0;
        while($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new Entretien($row["numEntretien"]);
            $counter++;
        }
        return $array;
    }

    //retourne un taleau avec tout les documents liés à cette réponse
    public function tousSesDocs()
    {
        $query = db()->prepare('SELECT numDoc FROM documentreponseoffre WHERE numReponseOffre = ?');
        $query->execute([$this->numReponseOffre]);

        $array = [];
        $counter = 0;
        while($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new Document($row["numDoc"]);
            $counter++;
        }
        return $array;

    }
    /*methodes statiques*/

    //ajoute une reponseoffre et renvoie un objet contenant les données de cette reponseoffre
    public static function ajouteReponseOffre($numCandidat, $numOffre, $accepte, $motifRefus=null)
    {
        $myDb = db();

        $query = $myDb->prepare("INSERT INTO reponseoffre (numCandidat, numOffre, accepte, motifRefus) VALUES (?, ?, ? ,?)");
        $query->execute([$numCandidat, $numOffre, $accepte, $motifRefus]);

        return new ReponseOffre($numCandidat, $numOffre);
    }

    public function ajouterDoc($numDoc)
    {
        // inserer $numDoc et $this->numReponseOffre dans la table documentreponseoffre
        $query = db()->prepare("INSERT INTO documentreponseoffre(numReponseOffre, numDoc)VALUES (?, ?)");
        $query->execute([$this->numReponseOffre, $numDoc]);

    }

}