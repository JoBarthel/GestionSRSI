 <?php

class FicheDeSuivi{
	public $numFicheSuivi, $numOffre, $numCandidat,$remarque;

	/*
	 * Usage:
	 * $fiche = new FicheDeSuivi($numFiche)
	 * ou
	 * $fiche = new FicheDeSuivi($numOffre, $numCandidat)
	 */
	public function __construct(){

	    $numargs = func_num_args();
	    $arguments = func_get_args();

        //__contruct($numOffre, $numCandidat)
        if($numargs == 2) {
            $query = db()->prepare("SELECT * FROM fichesuivi WHERE numOffre = ? AND numCandidat = ?");
            $query->execute([$arguments[0], $arguments[1]]);

            if ($query->rowCount() != 1) throw new Exception("Erreur : fichesuivi introuvable");

            $fiche = $query->fetch();

            foreach (['numFicheSuivi', 'numOffre', 'numCandidat', 'remarque'] as $attr)
                $this->$attr = $fiche->$attr;
        }

        //__contruct($numFiches)
        else if($numargs == 1)
        {
            $query = db()->prepare("SELECT * FROM fichesuivi WHERE numFicheSuivi = ?");
            $query->execute([$arguments[0]]);

            if($query->rowCount() != 1) throw new Exception("Erreur : fichedesuivi #".$arguments[0]." introuvable");

            $fiche = $query->fetch();

            foreach (['numFicheSuivi', 'numOffre', 'numCandidat', 'remarque'] as $attr)
                $this->$attr = $fiche->$attr;
        }
        else echo "FicheDeSuivi() prend 1 ou 2 arguments";

	}

	public function ajouteFicheDeSuivi($numOffre, $numCandidat,$remarque=null)
	{
		$myDb = db();

        $query = $myDb->prepare("INSERT INTO fichesuivi (numOffre, numCandidat,remarque) VALUES (?, ?, ?)");
        $query->execute([$numOffre, $numCandidat,$remarque]);

        return new FicheDeSuivi($numOffre, $numCandidat);
	}

	public function supprime()
	{
		$query = db()->prepare("DELETE FROM fichesuivi WHERE numFicheSuivi = ?");
        $query->execute([$this->numFicheSuivi]);
	}

	public function toutesSesRelances(){

        $query = db()->prepare("SELECT numRelance FROM relance WHERE numFicheSuivi = ?");
        $query->execute([$this->numFicheSuivi]);

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new Relance($row["numRelance"]);
            $counter++;
        }

        return $array;
	}

	//modifie la remarque tel que passé en paramètres
    public function setRemarque($remarque)
    {
        $query = db()->prepare("UPDATE fichesuivi SET remarque = ? WHERE numFicheSuivi = ?");
        $query->execute([$remarque, $this->numFicheSuivi]) or die("Erreur : la remarque n'a pas pu être mise à jour dans la bdd");

        $this->remarque = $remarque;
    }



}

?>