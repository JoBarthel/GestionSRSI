<?php

/**
 * Created by PhpStorm.
 * User: Joséphine
 * Date: 24/12/2016
 * Time: 22:38
 */
class OffreEmbauche
{
    public $numOffre, $poste, $societe, $adresse, $nomResponsable, $mailResponsable, $telResponsable, $description, $instructionsPostule, $dateOffre;


    public function __construct($numOffre)
    {
        $query = db()->prepare("SELECT * FROM offreembauche WHERE numOffre = ?");
        $query->execute([$numOffre]);

        if($query->rowCount() != 1) throw new Exception("Erreur : offre introuvable #".$numOffre);

        $offre = $query->fetch();

        foreach(['numOffre', 'poste', 'societe', 'adresse', 'nomResponsable', 'mailResponsable', 'telResponsable', 'description','instructionsPostule', 'dateOffre'] as $attr)
            $this->$attr = $offre->$attr;

    }

    /*Methodes des Offres*/

    //supprime cette offre
    public function supprimerOffre()
    {
        $query = db()->prepare("DELETE FROM offreembauche WHERE numOffre = ?");
        $query->execute([$this->numOffre]);
    }

    //retourne un tableau avec les numéros tout les candidats ayant répondu à cette offre
    public function candidatsAyantRepondu()
    {
        $query = db()->prepare("SELECT numCandidat FROM candidat NATURAL JOIN reponseoffre NATURAL JOIN offreembauche WHERE numOffre = ? ");
        $query->execute([$this->numOffre]);

        $array = [];
        $counter = 0;
        while($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = $row["numCandidat"];
            $counter++;
        }

        return $array;
    }

    //renvoie tout les docs liés à cette offre
    public function tousSesDocs()
    {
        $query = db()->prepare("SELECT numDoc FROM document WHERE numOffre = ?");
        $query->execute([$this->numOffre]);

        $array=[];
        $counter = 0;

        while ($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new Document($row['numDoc']);
            $counter++;
        }

        return $array;
    }

    /*Méthodes statiques*/

    //ajoute une offre et renvoie un objet contenant les données de cette offre
    public static function ajouterOffre($poste, $societe, $adresse, $nomResponsable, $mailResponsable, $telResponsable, $description,$instructions, $dateOffre)
    {
        $myDb = db();
        $query = $myDb->prepare("INSERT INTO offreembauche (poste, societe, adresse, nomResponsable, mailResponsable, telResponsable, description, instructionsPostule,dateOffre) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $query->execute([$poste, $societe, $adresse, $nomResponsable, $mailResponsable, $telResponsable, $description,$instructions, $dateOffre]);

        return new OffreEmbauche($myDb->lastInsertId());
    }

    //fait un tableau de OffreEmbauche contenant toutes les offres d'embauche
    public static function listerOffres()
    {
        $query = db()->prepare("SELECT numOffre FROM offreembauche");
        $query->execute();

        $array = [];
        $counter = 0;
        while($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new OffreEmbauche($row["numOffre"]);
            $counter++;
        }
        return $array;

    }
}