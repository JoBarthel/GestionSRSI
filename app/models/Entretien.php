<?php

/**
 * Created by PhpStorm.
 * User: Joséphine
 * Date: 24/12/2016
 * Time: 23:33
 */
class Entretien
{
    public $numEntretien, $dateEntretien, $resumeEntretien, $numOffre, $numCandidat;

    public function __construct($numEntretien)
    {
        $query = db()->prepare("SELECT * FROM entretien WHERE numEntretien = ?");
        $query->execute([$numEntretien]);

        if($query->rowCount() != 1) throw new Exception("Erreur : entretien #".$numEntretien." introuvable");

        $entretien = $query->fetch();

        foreach (['numEntretien', 'dateEntretien', 'resumeEntretien', 'numOffre', 'numCandidat'] as $attr)
            $this->$attr = $entretien->$attr;
    }

    //supprime cet entretien
    public function supprimeEntretien()
    {
        $query = db()->prepare("DELETE FROM entretien WHERE numEntretien = ?");
        $query->execute([$this->numEntretien]);
    }

    //met à jour cet entretien
    public function updateEntretien($date, $resume)
    {
        $date = date('Y-m-d', strtotime($date));

        $this->dateEntretien = $date;
        $this->resumeEntretien = $resume;

        $query = db()->prepare("UPDATE entretien SET dateEntretien = ?, resumeEntretien = ? WHERE numEntretien = ?");
        $query->execute([$date, $resume, $this->numEntretien]);
    }
    /*methodes statiques*/

    //ajoute un Entretien et renvoie un objet contenant les données de cet entretien
    public static function ajouteEntretien($dateEntretien, $resumeEntretien, $numOffre, $numCandidat)
    {
        $myDb = db();
        $dateEntretien = date('Y-m-d', strtotime($dateEntretien));
        $query = $myDb->prepare("INSERT INTO entretien (numOffre, numCandidat, dateEntretien, resumeEntretien) VALUES (?, ?, ?, ?)");
        $query->execute([$numOffre, $numCandidat, $dateEntretien, $resumeEntretien]);

        return new Entretien($myDb->lastInsertId());
    }
}