<?php

/**
 * Created by PhpStorm.
 * User: Joséphine
 * Date: 24/12/2016
 * Time: 23:49
 */
class InfoPublique
{
    public $numInfo, $dateInfo, $texteInfo;

    public function __construct($numInfo)
    {
        $query = db()->prepare("SELECT * FROM infopublique WHERE numInfo = ?");
        $query->execute([$numInfo]);

        if($query->rowCount() != 1) throw new Exception("Erreur : infoPublique #".$numInfo." introuvable");

        $infoPublique = $query->fetch();

        foreach (['numInfo', 'dateInfo', 'texteInfo'] as $attr)
            $this->$attr = $infoPublique->$attr;

    }

    //supprime cette info
    public function supprime()
    {
        $query = db()->prepare("DELETE FROM infopublique WHERE numInfo = ?");
        $query->execute([$this->numInfo]);
    }

    //fait une liste de Document liés à cette info
    public function TousSesDocs()
    {
        $query = db()->prepare("SELECT numDoc FROM document WHERE numInfo = ?");
        $query->execute([$this->numInfo]);

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new Document($row["numDoc"]);
            $counter++;
        }

        return $array;
    }

    /*methodes statiques*/

    //ajoute une infipublique et renvoie un objet contenant les données de cette info
    public static function ajoute($dateInfo, $texteInfo)
    {
        $myDb = db();

        $query = $myDb->prepare("INSERT INTO infopublique (dateInfo, texteInfo) VALUES (?, ?)");
        $query->execute([$dateInfo, $texteInfo]);

        return new InfoPublique($myDb->lastInsertId());
    }

    //retourne un tableau d'InfoPublique qui contient toutes les infos
    public static function listerInfos()
    {
        $query = db()->prepare("SELECT numInfo, dateInfo FROM infopublique ORDER BY dateInfo DESC");
        $query->execute();

        $array = [];
        $counter = 0;
        while($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new InfoPublique($row["numInfo"]);
            $counter++;

        }
        return $array;
    }
}