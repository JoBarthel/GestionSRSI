<?php

/**
 * Created by PhpStorm.
 * User: Joséphine
 * Date: 24/12/2016
 * Time: 23:49
 */
class TypeDoc
{
    public $numType, $nomType;

    public function __construct($numType)
    {
        $query = db()->prepare("SELECT * FROM typeDoc WHERE numType= ?");
        $query->execute([$numType]);

        if($query->rowCount() != 1) throw new Exception("Erreur : typeDoc #".$numType." introuvable");

        $statutDoc = $query->fetch();

        foreach (['numType', 'nomType'] as $attr)
            $this->$attr = $statutDoc->$attr;
    }

    //supprime cet entretien
    public function supprime()
    {
        $query = db()->prepare("DELETE FROM typeDoc WHERE numType = ?");
        $query->execute([$this->numType]);
    }

    /*methodes statiques*/

    //retourne la liste des type de documents
    public function lister()
    {
        $query = db()->prepare("SELECT numType FROM typeDoc");
        $query->execute();

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {

            $array[$counter] = new TypeDoc($row['numType']);
            $counter++;
        }
        return $array;
    }

    //ajoute une reponseoffre et renvoie un objet contenant les données de cette reponseoffre
    public static function ajoute($nomType)
    {
        $myDb = db();

        $query = myDb()->prepare("INSERT INTO typeDoc (nomType) VALUES (?)");
        $query->execute([$nomType]);

        return new TypeDoc($myDb->lastInsertId());
    }
}