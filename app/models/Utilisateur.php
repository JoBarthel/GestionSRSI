<?php


/* Cette classe contient les données de connexion d'un utilisateur.
    Chaque candidat correspond soit à un candidat soit à un responsable dans la base de données.
*/

abstract class Utilisateur
{
    /**
     * Partie statique
     */

    // Vérifie si l'utilisateur est dans la BD
    public static function verifier($login, $mdp)
    {
        //vérifie si l'utilisateur est dans la bd
        $query = db()->prepare('SELECT numUtilisateur, motDePasse FROM utilisateur WHERE login = ?');
        $query->execute([$login]);

        if ($query->rowCount() != 0) {
            $result = $query->fetch(PDO::FETCH_ASSOC);

            $motDePasse = $result['motDePasse'];

            if (password_verify($mdp, $motDePasse)) return $result['numUtilisateur'];
            else return false;
        } else return false;
    }

    // Retourne un objet Candidat ou Responsable à partir d'un numUtilisateur
    public static function get($numUtilisateur)
    {
        $query = db()->prepare('SELECT numCandidat, numResponsable FROM utilisateur WHERE numUtilisateur = ?');
        $query->execute([$numUtilisateur]);

        if ($query->rowCount() != 1) throw new Exception("Utilisateur introuvable #" . $numUtilisateur);

        $utilisateur = $query->fetch();

        $o = empty($utilisateur->numCandidat) ? new Responsable($utilisateur->numResponsable) : new Candidat($utilisateur->numCandidat);
        return $o;
    }

    public static function genererLogin($prenom, $nom)
    {
        $prenom = enleveAccent($prenom);
        $nom = enleveAccent($nom);
        $prenom = preg_replace('/[^A-Za-z0-9\.]/', '', $prenom);
        $nom = preg_replace('/[^A-Za-z0-9\-]/', '', $nom);
        $login = $prenom . '.' . $nom;
        $login = preg_replace('/[\b\s]/', '', $login);
        $login = strtolower($login);

        //si le login est pris ajoute un suffixe
        if (self::loginPris($login)) {
            $suffixe = 1;
            while (self::loginPris($login . $suffixe)) {
                $suffixe += 1;
            }
            $login = $login . $suffixe;
        }

        return $login;
    }

    //génére un nouveau mot de passe, composé de 10 chiffres et lettres aléatoires
    public static function genererMotDePasse()
    {
        return random_str(10);
    }

    //retourne le mail du responsable qui porte ce nom
    public static function contacterMail($nom)
    {
        $query = db()->prepare("SELECT mail FROM utilisateur WHERE nom = ?");
        $query->execute([$nom]);

        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result["mail"];
    }

    /**
     * Partie objet (héritée par Candidat et Responsable)
     */

    public $numUtilisateur, $prenom, $login, $nom, $motDePasse, $mail, $type;

    public function __construct($numUtilisateur, $type)
    {
        $query = db()->prepare('SELECT * FROM utilisateur WHERE numUtilisateur = ?');
        $query->execute([$numUtilisateur]);

        if ($query->rowCount() != 1) throw new Exception("Utilisateur introuvable #" . $numUtilisateur);

        $utilisateur = $query->fetch();

        // On injecte les colonnes de la base de données dans les attributs de l'objet
        foreach (['numUtilisateur', 'prenom', 'nom', 'login', 'motDePasse', 'mail'] as $attr) {
            $this->$attr = $utilisateur->$attr;
        }

        $this->type = $type;
    }

    //met cet utilisateur à jour dans la bdd 
    public function updateUtilisateur($motDePasse, $mail)
    {
        //hash le mot de passe
        $motDePasse = password_hash($motDePasse, PASSWORD_DEFAULT);

        //met cet objet à jour
        $this->motDePasse = $motDePasse;
        $this->mail = $mail;

        //met la bdd à jour
        $query = db()->prepare("UPDATE utilisateur SET motDePasse = ?, mail = ? WHERE numUtilisateur = ?");
        $query->execute([$motDePasse, $mail, $this->numUtilisateur]) or die("Le statut " . $this->numUtilisateur . " n'a pas été mis à jour correctement.");
    }

    //crée l'utilisateur du type $type dans la bdd correspondant à ces informations et retourne son numéro
    public static function ajouteUtilisateur($nom, $prenom, $mail, $mdp)
    {

        // On génère un login
        $nouveauLogin = self::genererLogin($prenom, $nom);

        //crée le nouvel utilisateur
        $myDB = db();

        //hash le mot de passe
        $nouveauMotDePasseHashe = password_hash($mdp, PASSWORD_DEFAULT);


        //insère les infos dans la base de données
        $query = $myDB->prepare("INSERT INTO utilisateur (`nom`, `prenom`, `login`, `motDePasse`, `mail`) VALUES (?, ?, ?, ?, ?)");
        $query->execute([$nom, $prenom, $nouveauLogin, $nouveauMotDePasseHashe, $mail]);

        return $myDB->lastInsertId();

    }

    //vérifie si ce login est déjà utilisé
    public static function loginPris($login)
    {
        $query = db()->prepare("SELECT numUtilisateur FROM utilisateur WHERE login = ?");
        $query->execute([$login]);

        return ($query->rowCount() > 0) ? true : false;
    }

    public function estCandidat()
    {
        return $this->type == 'candidat';
    }

    public function estResponsable()
    {
        return $this->type == 'responsable';
    }

    public function getNomComplet()
    {
        return $this->prenom . ' ' . $this->nom;
    }
}