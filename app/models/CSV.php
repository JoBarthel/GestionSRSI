<?php

/**
 * Class CSV
 * @Author Maxime Renou
 */

class CSV
{
    protected $lines;
    protected $index;
    protected $columns = [];

    public function __construct($contents)
    {
        $this->parse($contents);
        $this->index = 0;
    }

    protected function parse($contents)
    {
        $lines = preg_split("/((\r?\n)|(\r\n?))/", $contents);
        $this->lines = array_map('str_getcsv', $lines);
        foreach (array_shift($this->lines) as $i => $column) {
            $this->columns[$i] = strtolower($column);
        }
    }

    public function next()
    {
        $this->index++;
        if (count($this->lines) < $this->index) return false;
        return $this->lines[$this->index - 1];
    }

    public function columns()
    {
        return $this->columns;
    }

    public function get($name)
    {
        if (is_array($name)) {
            foreach ($name as $c) {
                foreach ($this->columns as $i => $column) {
                    if ($c == $column) return @$this->lines[$this->index - 1][$i];
                }
            }
        } else {
            $i = array_search($name, $this->columns);
            if ($i !== false) {
                return $this->lines[$this->index - 1][$i];
            }
        }

        return false;
    }
}