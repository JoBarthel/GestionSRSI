<?php

class StatutFicheSuivi {

    public $numStatut;
    public $nomStatut;

    //cherche les infos dans la base de données qui correspondent à ce numéro et met les infos à jour
    //attention rentrer un numéro qui n'existe pas dans la db crée une fatal error
    public function __construct($numStatut)
    {
        $this->numStatut = $numStatut;

        $query = db()->prepare('SELECT * FROM statutfichesuivi WHERE numStatut = ?');
        $query->execute([$numStatut]);

        if($query->rowCount() != 1) throw new Exception("Statut introuvable #".$numStatut);

        $statut = $query->fetch();

        //injecte chaque colonne de Statut dans les attributs
        foreach (['numStatut', 'nomStatut'] as $attr) {
            $this->$attr = $statut->$attr;
        }

    }


    /*

    PARTIE STATIQUE

    */

    //retourne la liste des statuts en un PDOStatement
    /* Exemple pour récupérer les résultats

    $listeStatut = ControleStatut::listerStatuts();

    while($row = $listeStatut->fetch(PDO::FETCH_ASSOC))
    {
        echo $row["numStatut"].' : '.$row["nomStatut"].' '.$row["descStatut"].'</br>';
    }
    */
    public static function lister()
    {
        $query = db()->prepare('SELECT numStatut FROM statut');
        $query->execute();

        $array=[];
        $counter = 0;

        while($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new StatutFicheSuivi($row['numStatut']);
            $counter++;
        }

        return $array;


    }



}