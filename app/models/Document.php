<?php 

class Document {

	public $numDoc, $nomDoc, $statut, $dateMod, $chemin, $type, $numUtilisateur, $numReponseOffre, $public, $numInfo, $numRaison, $emplacementDoc, $numOffre;
	public static $numStatutDefaut = 3;

	public function __construct($numDoc)
	{
		$this->numDoc = $numDoc;

		$query = db()->prepare("SELECT * FROM document WHERE numDoc = ?");
		$query->execute([$numDoc]);

		if($query->rowCount() != 1) throw new Exception("Erreur : document #".$numDoc." introuvable.");

		$document = $query->fetch();

		//injecte chaque colonne de Statut dans les attributs
		foreach (['numDoc', 'nomDoc', 'numStatut', 'dateMod', 'chemin', 'type','numUtilisateur', 'numReponseOffre', 'public', 'numInfo', 'numRaison', 'emplacementDoc', 'numOffre'] as $attr) {
			$this->$attr = $document->$attr;
		}
		

	}

	//met à jour les données de ce document dans la base de données et dans cet objet. Remet le statut à numStatutDefaut
	public function updateDocument($nomDoc, $chemin, $emplacement)
	{
		$this->nomDoc = $nomDoc;
		$this->chemin = $chemin;

		$this->dateMod = date('Y-m-d');

		$this->statut = Document::$numStatutDefaut;

		$query = db()->prepare("UPDATE document SET nomDoc = ?, dateMod = ?, chemin = ?, emplacementDoc = ?, numStatut = ? WHERE numDoc = ?");
		$query->execute([$nomDoc, $this->dateMod, $chemin, $emplacement,Document::$numStatutDefaut, $this->numDoc]);
	}
	
	//supprime ce document 
	public function supprimeDocument()
	{

        $query = db()->prepare("DELETE FROM document WHERE numDoc = ?");
        $query->execute([$this->numDoc]) or die("Le document".$this->numDoc." n'a pas supprimé correctement.") ;
	}


	//télécharge ce document
	public function telecharger()
	{
		return "<div class =\"document\"><a href='telechargement.php?path=".$this->chemin."'>".$this->nomDoc." </a></div> </br>";
	}

	//retourne le statut de ce document
    public function getStatut()
    {
        $query = db()->prepare("SELECT numStatut FROM statutDoc NATURAL JOIN document WHERE numDoc = ?");
        $query->execute([$this->numDoc]);

        if($query->rowCount() != 1) throw new Exception("Erreur : le statut du document#".$this->numDoc." est introuvable");

        return new StatutDoc($query->fetch(PDO::FETCH_ASSOC)["numStatut"]);
    }

    //change le statut de ce document
    public function setStatut($nouveauStatut)
    {
        $query = db()->prepare("UPDATE document SET numStatut = ? WHERE numDoc = ? ");
        $query->execute([$nouveauStatut, $this->numDoc]) or die("Erreur : statut introuvable #".$nouveauStatut);
    }

    //change la raison de refus de ce doc
    public function setRaison($nouvelleRaison)
    {
        $query = db()->prepare("UPDATE document SET numRaison = ? WHERE numDoc = ?");
        $query->execute([$nouvelleRaison, $this->numDoc]);
    }

    //affiche la raison du refus de ce document
    public function  getRaison($nouvelleRaison)
    {
        $query = db()->prepare("SELECT nomRaison FROM RAISON WHERE numRaison = ?");
        $query = db()->execute([$nouvelleRaison]);

        return $query->fetch(PDO::FETCH_ASSOC)["nomRaison"];
    }

    //modifier l'info à laquelle appartient le document
    public function setInfo($numInfo)
    {
        $this->numInfo = $numInfo;

        $query = db()->prepare("UPDATE document SET numInfo = ? WHERE numDoc = ?");
        $query->execute([$numInfo, $this->numDoc]);


    }

    //modifier l'offre à laquelle appartient le document
    public function setOffre($numOffre)
    {
        $this->numOffre = $numOffre;

        $query = db()->prepare("UPDATE document SET numOffre = ? WHERE numDoc = ?");
        $query->execute([$numOffre, $this->numDoc]);


    }

    //retourne vrai si le document a le statut Validé
    public function docValide()
    {

    }
    //modifie la raison du refus de ce document
	/* Méthodes statiques */

	//fait la liste des documents disponibles
	/*Usage :    
	$listeDoc =  Document::listerDocuments();
    while($row = $listeDoc->fetch(PDO::FETCH_ASSOC)) echo $row["nomDoc"]." ".$row["statut"]."</br>";
    */

	public static function listerDocuments()
	{
		$query = db()->prepare("SELECT * FROM document");
		$query->execute();

		return $query;
	}

	//ajoute le document dans la BDD. Retourne l'objet avec les valeurs correspondantes	
	public static function ajouteDocument($nomDoc, $numUtilisateur, $type, $chemin, $public, $emplacement, $numInfo=null, $numReponseOffre=null)
	{
		$myDB = db();

		//date_default_timezone_set('Europe/Paris');
		$dateMod = date('Y-m-d');

		//crée un hash à partir du nom de l'utilisateur et de la date
        $user = Utilisateur::get($numUtilisateur);

		$query = $myDB->prepare("INSERT INTO document(`nomDoc`, `numUtilisateur`, `type`, `dateMod`, `numStatut`, `chemin`, `public`, `numReponseOffre`, `numInfo`, `emplacementDoc`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$query->execute([$nomDoc, $numUtilisateur, $type, $dateMod, Document::$numStatutDefaut, $chemin, $public, $numReponseOffre, $numInfo, $emplacement]);

		return new Document($myDB->lastInsertId());
	}

	//retourne un tableau contenant les numéros de tous les docs de l'utilisateur passé en paramètres
	public static function utilisateurDoc($numUtilisateur)
	{
		$query = db()->prepare("SELECT numDoc FROM document WHERE numUtilisateur = ?");
		$query->execute([$numUtilisateur]);
		$counter = 0;

		$array = [];
		while($row = $query->fetch(PDO::FETCH_ASSOC)) 
		{
			$array[$counter] = $row["numDoc"];
			$counter++;
		}

		return $array;
	}
}