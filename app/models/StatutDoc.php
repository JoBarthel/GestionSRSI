<?php

/**
 * Created by PhpStorm.
 * User: Joséphine
 * Date: 24/12/2016
 * Time: 23:49
 */
class StatutDoc
{
    public $numStatut, $nomStatut, $descStatut;

    public function __construct($numStatut)
    {
        $query = db()->prepare("SELECT * FROM statutdoc WHERE numStatut= ?");
        $query->execute([$numStatut]);

        if($query->rowCount() != 1) throw new Exception("Erreur : statutDoc #".$numStatut." introuvable");

        $statutDoc = $query->fetch();

        foreach (['numStatut', 'nomStatut', 'descStatut'] as $attr)
            $this->$attr = $statutDoc->$attr;
    }

    //supprime cet entretien
    public function supprime()
    {
        $query = db()->prepare("DELETE FROM statutdoc WHERE numStatut = ?");
        $query->execute([$this->numStatut]);
    }

    /*methodes statiques*/

    //retourne le numéro du statut qui correspond à validé
    public static function numStatutValideActuel()
    {
        return 1;
    }

    //retourne le numéro du statut qui correspond à refusé
    public static function numStatutRefuseActuel()
    {
        return 4;
    }


    //ajoute une reponseoffre et renvoie un objet contenant les données de cette reponseoffre
    public static function ajoute($nomStatut, $descStatut)
    {
        $myDb = db();

        $query = myDb()->prepare("INSERT INTO statutdoc (nomStatut, descStatut) VALUES (?, ?)");
        $query->execute([$nomStatut, $descStatut]);

        return new StatutDoc($myDb->lastInsertId());
    }
}