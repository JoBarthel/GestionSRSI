<?php

/**
 * Created by PhpStorm.
 * User: Joséphine
 * Date: 24/12/2016
 * Time: 23:49
 */
class RaisonRefus
{
    public $numRaison, $nomRaison, $descRaison;

    public function __construct($numRaison)
    {
        $query = db()->prepare("SELECT * FROM raisonrefus WHERE numRaison = ?");
        $query->execute([$numRaison]);

        if($query->rowCount() != 1) throw new Exception("Erreur : RaisonRefus #".$numRaison." introuvable");

        $raisonRefus = $query->fetch();

        foreach (['numRaison', 'nomRaison', 'descRaison'] as $attr)
            $this->$attr = $raisonRefus->$attr;
    }

    //supprime cet entretien
    public function supprime()
    {
        $query = db()->prepare("DELETE FROM raisonrefus WHERE numRaison = ?");
        $query->execute([$this->numRaison]);
    }

    /*methodes statiques*/

    //ajoute une raison refus et renvoie un objet contenant les données de cette reponseoffre
    public static function ajoute($nomRaison, $descRaison)
    {
        $myDb = db();

        $query = $myDb->prepare("INSERT INTO raisonrefus (nomRaison, descRaison) VALUES (?, ?)");
        $query->execute([$nomRaison, $descRaison]);

        return new RaisonRefus($myDb->lastInsertId());
    }

    //retourne la liste des numéros des raisonRefus
    public static function lister()
    {
        $query = db()->prepare("SELECT numRaison FROM raisonrefus");
        $query->execute();

        $array=[];
        $counter = 0;
        while($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = $row['numRaison'];
            $counter++;
        }
        return $array;
    }
}