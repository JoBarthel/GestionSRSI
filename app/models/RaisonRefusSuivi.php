<?php

/**
 * Created by PhpStorm.
 * User: Joséphine
 * Date: 24/12/2016
 * Time: 23:49
 */
class RaisonRefusSuivi
{
    public $numRaison, $nomRaison;

    public function __construct($numRaison)
    {
        $query = db()->prepare("SELECT * FROM raisonrefussuivi WHERE numRaison = ?");
        $query->execute([$numRaison]);

        if($query->rowCount() != 1) throw new Exception("Erreur : RaisonRefus #".$numRaison." introuvable");

        $raisonRefus = $query->fetch();

        foreach (['numRaison', 'nomRaison'] as $attr)
            $this->$attr = $raisonRefus->$attr;
    }

    //supprime cet entretien
    public function supprime()
    {
        $query = db()->prepare("DELETE FROM raisonrefussuivi WHERE numRaison = ?");
        $query->execute([$this->numRaison]);
    }

    /*methodes statiques*/

    //ajoute une raison refus et renvoie un objet contenant les données de cette reponseoffre
    public static function ajoute($nomRaison)
    {
        $myDb = db();

        $query = $myDb->prepare("INSERT INTO raisonrefussuivi (nomRaison) VALUES (?)");
        $query->execute([$nomRaison]);

        return new RaisonRefusSuivi($myDb->lastInsertId());
    }

    //retourne la liste des raisonRefusSuivi
    public static function lister()
    {
        $query = db()->prepare("SELECT numRaison FROM raisonrefussuivi");
        $query->execute();

        $array=[];
        $counter = 0;
        while($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $array[$counter] = new RaisonRefusSuivi($row['numRaison']);
            $counter++;
        }
        return $array;
    }
}