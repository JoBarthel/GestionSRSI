<?php

/**
 * Created by PhpStorm.
 * User: Joséphine
 * Date: 01/01/2017
 * Time: 20:30
 */
class Relance
{
    public $numRelance, $dateRelance, $typeRelance, $numFicheSuivi;

    public function __construct($numRelance)
    {
        $query = db()->prepare("SELECT * FROM relance WHERE numRelance = ?");
        $query->execute([$numRelance]);

        if($query->rowCount() != 1) throw new Exception("Erreur : relance #".$numRelance." introuvable");

        $relance = $query->fetch();

        foreach (['numRelance', 'dateRelance', 'typeRelance', 'numFicheSuivi'] as $attr)
            $this->$attr = $relance->$attr;

    }

    //supprime cette relance
    public function supprime()
    {
        $query = db()->prepare("DELETE FROM relance WHERE numRelance = ?");
        $query->execute([$this->numRelance]);
    }

    /*methodes statiques*/

    //ajoute une relance et renvoie un objet contenant les données de cette relance
    public static function ajoute($dateRelance, $typeRelance, $numFicheSuivi)
    {
        $myDb = db();

        $query = $myDb->prepare("INSERT INTO relance (dateRelance, typeRelance, numFicheSuivi) VALUES (?, ?, ?)");
        $query->execute([$dateRelance, $typeRelance, $numFicheSuivi]);

        return new Relance($myDb->lastInsertId());
    }


}