<?php

/* Cette classe contient les données personnelles d'un utilisateur de type responsable */

class Responsable extends Utilisateur
{
    public $numResponsable, $alerteSurReset, $alerteSurNouveauCandidat;

    public function __construct($numResponsable)
    {
        $query = db()->prepare('SELECT * FROM responsable WHERE numResponsable = ?');
        $query->execute([$numResponsable]);

        if ($query->rowCount() != 1) throw new Exception("Responsable introuvable #" . $numResponsable);

        $responsable = $query->fetch();

        // On appelle le constructeur de Utilisateur
        parent::__construct($responsable->numUtilisateur, 'responsable');

        // On injecte chaque colonne de la table Responsable dans un attribut
        foreach (['numResponsable', 'alerteSurReset', 'alerteSurNouveauCandidat'] as $attr) {
            $this->$attr = $responsable->$attr;
        }
    }

    /* Ici on peut placer des méthodes pour les utilisateur de type Responsable */

    //supprime ce responsable de la base de donnée (et l'utilisateur corresponsant)
    public function supprimeResponsable()
    {

        $query = db()->prepare("DELETE FROM responsable WHERE responsable = ?");
        $query->execute([$this->numResponsable]) or die("Le responsable " . $this->numResponsable . " n'a pas supprimé correctement.");

        $query = db()->prepare("DELETE FROM utilisateur WHERE numUtilisateur = ?");
        $query->execute([$this->numUtilisateur]) or die("L utilisateur " . $this->numUtilisateur . " n'a pas supprimé correctement.");
    }

    //setAlertes($alerteSurReset, $alerteSurNouveauCandidat)
    //prend en paramètres 1 à 2 booleen et active l'alerte sur vrai, la désactive sur faux
    public function setAlertes($alerteSurReset, $alerteSurNouveauCandidat)
    {

        $query = db()->prepare("UPDATE responsable SET alerteSurReset = ? WHERE numResponsable = ?");
        $query->execute([$alerteSurReset, $this->numResponsable]);


        $query = db()->prepare("UPDATE responsable SET alerteSurNouveauCandidat = ? WHERE numResponsable = ?");
        $query->execute([$alerteSurNouveauCandidat, $this->numResponsable]);

    }

    /*Méthodes statiques */

    //renvoie la liste des rRsponsables sous forme d'un résultat
    public static function listerResponsables()
    {
        $query = db()->prepare("SELECT numResponsable FROM responsable NATURAL JOIN utilisateur");
        $query->execute();

        $array = [];
        $counter = 0;
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $array[$counter] = new Responsable($row['numResponsable']);
            $counter++;
        }
        return $array;
    }

    //ajoute le responsable correspondant à ces informations (et l'utilisateur corresponsandant) et retourne l'objet corresponsant à ces données
    public static function ajouteResponsable($nom, $prenom, $mail)
    {
        $myDB = db();

        //génère un mot de passe de 10 chiffres aléatoire
        $nouveauMotDePasse = Utilisateur::genererMotDePasse();

        //crée l'utilisateur correspondant et stocke son ID 
        $utilId = Utilisateur::ajouteUtilisateur($nom, $prenom, $mail, $nouveauMotDePasse);

        //ajoute le responsable dans la BDD avec le numéro d'utilisateur correspondant
        $query = $myDB->prepare("INSERT INTO responsable (`numUtilisateur`) VALUES (?)");
        $query->execute([$utilId]);

        //récupère le numResponsable et l'insere dans la table utilisateur, à la ligne correspondante à ce responsable
        $responsableId = $myDB->lastInsertId();
        $query = $myDB->prepare("UPDATE utilisateur SET numResponsable = ? WHERE numUtilisateur = ?");
        $query->execute([$responsableId, $utilId]);

        //crée un objet avec les infos du responsable
        $responsable = new Responsable($responsableId);

        //envoie un mail au responsable avec son mot de passe non haché
        notify($responsable, 'confirmation-inscription', $nouveauMotDePasse);

        //affiche les identifiants du compte à l'écran
        flash("Login :".$responsable->login." Mot de passe : ".$nouveauMotDePasse);

        //retourne ce responsable
        return $responsable;

    }

    //envoie une alerte à tout les responsables qui ont cette alerte activée
    public static function envoyerAlerte($nomAlerte)
    {
        foreach (['alerteSurReset', 'alerteSurNouveauCandidat'] as $alerte) {
            if ($nomAlerte == $alerte) {

                $liste = Responsable::listerResponsables();
                foreach ($liste as $responsable) {
                    if ($responsable->$nomAlerte == 1)
                        notify($responsable, $nomAlerte);
                }
            }
        }
    }


}