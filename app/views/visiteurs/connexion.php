<div class="info">
    <p> Bienvenue sur le site de la license SRSI de l'Université Paris-Saclay. Ici se déroulera la procédure de recrutement.
        Si vous êtes candidat : vos identifiants devraient vous avoir été envoyé par mail. La première fois que vous vous connecterez, vous devrez changer votre mot de passe
        Si vous êtes responsable : connectez-vous avec vos identifiants ADONIS.
    </p>
</div>
<form method="post" action="connexion.php" class="form">
    <p>
        <label for="username">Nom d'utilisateur</label>
        <input type="text" name="username" id="username">
    </p>
    <p>
        <label for="password">Mot de passe</label>
        <input type="password" name="password" id="password">
    </p>
    <p>
        <button>Connexion</button>
    </p>
</form>