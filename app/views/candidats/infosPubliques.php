<h1> Informations publiques </h1>

<?php

for($i = 0 ; $i < count($listeInfos) ; $i++)
{
    $info = $listeInfos[$i];
    echo "<h2 class=\"titre\"> Information du ".$info->dateInfo."</br> </h2>";
    echo "<p>".$info->texteInfo."</br> </p>";

    $listeDoc = $info->TousSesDocs();
    for($j = 0 ; $j < count($listeDoc) ; $j++)
        echo '<div class = "doc">'.$listeDoc[$j]->telecharger()."</div>";

    echo "</br>";
}