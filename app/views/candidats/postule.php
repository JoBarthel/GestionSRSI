<h1> Postuler </h1>
<?php
$listeDoc = Document::utilisateurDoc(Auth::user()->numUtilisateur);
$offre = $_GET['offre'];
$embauche = new OffreEmbauche($offre);

if(!empty($embauche->instructionsPostule)) echo "<h3>Instructions de Candidature : </br>".$embauche->instructionsPostule." </h3> ";
?>

<h2> Ajouter des documents</h2>
<form action="redirectPostule.php?offre=<?=$offre?>" method="post">
    <?php
    for ($i = 0; $i < count($listeDoc); $i++) {
        $doc = new Document($listeDoc[$i]);
        echo '<input type="checkbox" name="docs[]" value="' . $doc->numDoc . '"> ' . $doc->nomDoc . "</br>";
    }
    ?>
    </br>
    <input type="submit" name="Confirmer" value="Confirmer">
</form>

<a class="button" href="offre.php?offre=<?= $offre ?>">Annuler</a>