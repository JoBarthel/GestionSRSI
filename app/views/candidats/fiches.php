<div class="contenuAcceuil">

    <?php

    for ($i = 0; $i < count($listeOffre); $i++) {
        echo "<div class=\"offre\"><a href=\"ficheEnt.php?offre=" . $listeOffre[$i]->numOffre . "\">" . $listeOffre[$i]->societe . "
            <p>" . $listeOffre[$i]->poste . "</p> </a>
            </div>";
    }

    $utilisateur = Utilisateur::get(Auth::user()->numUtilisateur);
    if ($utilisateur->statutFicheSuivi == Candidat::numSuiviValide()) echo " <div class='contenu'> Fiche de suivi validée pour cette semaine </div>";
    else if ($utilisateur->statutFicheSuivi == Candidat::numSuiviRefuse()) echo " <div class='contenu'> Votre fiche de suivi n'a pas été validée cette semaine. Raison : ".(new RaisonRefusSuivi($utilisateur->numRaison))->nomRaison." </div>";
    else echo "<div class='contenu'> Fiche de suivi en attente de validation. </div>";

    echo "<div class='contenu'> Prochaine validation le: " . Candidat::prochaineInitialisation()." </div>";
    ?>
</div>