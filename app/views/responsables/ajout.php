<div class="ajout">
    <h2> Ajouter un candidat ou responsable</h2>
	<table>
		<form method="post" action="actions\fonctionAjout.php">
		<tr>
			<td>
				<label>Nom : </label>
			</td>
			<td>
				<input type="text" name="nom" id="nom"/>
			</td>
		</tr>
		<tr>
			<td>
				<label>Prénom : </label>
			</td>
			<td>
				<input type="text" name="prenom" id="prenom"/>
			</td>
		</tr>
		<tr>
			<td>
				<label>E-mail</label>
			</td>
			<td>
				<input type="text" name="mail" id="mail"/>
			</td>
		</tr>
		<tr>
			<td>
				<input type="radio" name="typeUser" id ="typeUser" value="Candidat">Candidat
			</td>
			<td>
				<input type="radio" name="typeUser" id ="typeUser" value="Responsable">Responsable
			</td>

		</tr>
			<td></td>
			<td>
				<input type="submit" name="envoyer" value="Valider" data-confirm="Êtes-vous sûr de vouloir ajouter cet utilisateur ?">
			</td>
		</form>
	</table>

    <h2> Ajouter des candidats à l'aide d'un fichier CSV</h2>

    <!-- Ajouter un fichier CSV -->
    <form enctype="multipart/form-data" action="actions/ajouterCandidatsCSV.php" method="POST">
        <input type="hidden" name="MAX_FILE_SIZE" value="100000"/>
        <input name="uploadedfile" type ="file" />
        <input type="submit" value="Ajouter ces candidats" data-confirm="Êtes-vous sûr de vouloir ajouter ces utilisateurs ?"/>
    </form>
</div>