
<h3> <?=date('Y-m-d')?></h3>

<!-- Ajouter un document -->
<form enctype="multipart/form-data" action="actions/ajouterInfoDoc.php" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="100000"/>
    <input name="uploadedfile" type ="file" />
    <input type="submit" value="Ajouter ce document"/>
</form>

<!-- Les documents de l'info-->
<?php $listeDoc = $_SESSION['listeDoc'];
        for($i = 0 ; $i < count($listeDoc) ; $i++) {

            $doc = new Document($listeDoc[$i]);
            echo "<div class='doc' >".$doc->telecharger()."</div>";
        }
        ?>

<!-- Le texte de l'info-->
<form method="post" action="actions/ajouterInfo.php">
    <p><textarea name="texteInfo" rows="8" cols="50" maxlength="400" placeholder="Votre message"></textarea> </p>

    <input type="submit" value="Ajouter cette information"/>
</form>

