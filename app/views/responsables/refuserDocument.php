
<!-- formulaire de refus (liste déroulante de nomDeRefus + valider -->
<h2> Indiquez la raison de votre refus</h2>
<form method="post" action="actions/refuserDocument.php">
    <select name="numRaison">
        <?php
            foreach ($listeRaisonRefus as $r)
            {
                $raison = new RaisonRefus($r);
                echo "<option value='".$r."'>".$raison->nomRaison."</option>";
            }
        ?>
    </select>
    <input type="hidden" name="numDoc" value="<?=$numDoc ?>">
    <input type="hidden" name="numCandidat" value="<?=$numCandidat?>">
    <input type="submit" value="Refuser ce document"/>

</form>

<!--formulaire d'ajout des refus (text + textare + valider -->
<h2> Ajouter une raison de refus</h2>
<h3> Utilisez ce formulaire si aucune raison de la liste çi-dessus ne vous convient</h3>
<form method="post" action="actions/ajouterRaisonRefus.php">
    <p> Nom de la raison <input type="text" name="nomRaison"/>  </p>
    <p>  <textarea name="descRaison" rows ="10" cols="40" maxlength="100" placeholder="Description de la raison ici.. "></textarea> </p>
    <input type="hidden" name="numDoc" value="<?=$numDoc?>" />
    <input type="hidden" name="numCandidat" value="<?=$numCandidat?>" />
    <p> <input type="submit" value="Ajouter une raison"></p>
</form>

