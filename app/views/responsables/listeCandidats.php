<h1> Liste des candidats</h1>

<!-- Parcours la liste des candidats. Si son fichier n'a pas été validé, l'affiche en premier. Sinon, le
stocke dans un tableau temporaire pour l'affiche ensuite-->
<h2 class="titre"> Candidats dont le formulaire de suivi est en attente de validation </h2>

<?php

for ($i = 0; $i < count($listeCandidat); $i++) {
    $candidat = new Candidat($listeCandidat[$i]);
    if ($candidat->statutFicheSuivi != Candidat::numSuiviEnAttente()) $candidatsValide[count($candidatsValide)] = $candidat;
    else echo '<div class="doc"> <a href="afficherCandidat.php?numCandidat=' . $candidat->numCandidat . '">' . $candidat->getNomComplet() . '</a> </div>';
}
?>


<h2 class="titre"> Candidats ayant validé leur formulaire de suivi</h2>

<?php
foreach ($candidatsValide as $c) {
    if($c->statutFicheSuivi != Candidat::numSuiviValide()) $candidatsRefuses[count($candidatsRefuses)] = $c;
    else echo '<div class="doc"> <a href="afficherCandidat.php?numCandidat=' . $c->numCandidat . '">'.$c->getNomComplet().' </a> </div>';
}

?>

<h2 class="titre"> Candidats dont le formulaire de suivi a été refusé</h2>
<?php
foreach ($candidatsRefuses as $c) {
    echo '<div class="doc"> <a href="afficherCandidat.php?numCandidat=' . $c->numCandidat .' ">'.$c->getNomComplet().' </a> </div>';
}
?>

<a class="button" href="rechercheCandidat.php"> Recherche </a>
