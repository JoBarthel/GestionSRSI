<h3> Fiche de suivi de <?= $candidat->getNomComplet() ?></h3>
    <?php

    for ($i = 0; $i < count($listeOffre); $i++) {
        echo "<div class='offre'><a href='ficheEnt.php?offre=" . $listeOffre[$i]->numOffre . "&candidat=" . $candidat->numCandidat . "'>" . $listeOffre[$i]->societe . "
            <p>" . $listeOffre[$i]->poste . "</p> </a>
            </div>";
    }

    if ($candidat->statutFicheSuivi == Candidat::numSuiviEnAttente()) { ?>
        </br>
        <p><a class="button" href='actions/validerFicheSuivi.php?numCandidat=<?= $candidat->numCandidat?>'> Valider la fiche de suivi pour cette semaine</a></p>
        </br>
        <h3> Sinon, sélectionnez dans la liste çi-dessous la raison de refus de la fiche </h3>
        <form method="post" action = "actions/refuserFicheSuivi.php">
        <select name = "numRaison">
            <?php
                foreach ($listeRaisonRefus as $raison)
                {
                    echo "<option value =".$raison->numRaison."> ".$raison->nomRaison." </option>";
                }
            ?>
        </select>
        <input type="hidden" name ="numCandidat" value="<?=$candidat->numCandidat?>">
        <input type="submit" value ="Refuser le formulaire">
        </form>


        <h3> Ou entrez une nouvelle raison avec le formulaire çi dessous</h3>
        <form method="post" action="actions/ajouterRaisonSuivi.php">
            <input type="text" name="nomRaison">
            <input type="hidden" name="numCandidat" value="<?=$candidat->numCandidat?>">
            <input type="submit" value="Ajouter cette raison">
        </form>
        <?php
    } else if($candidat->statutFicheSuivi == Candidat::numSuiviValide()) echo "Cette fiche de suivi a été validéé pour cette semaine";
      else echo "Cette fiche de suivi a été refusée cette semaine";
    ?>

    </br>
    <a class="button" href="afficherCandidat.php?numCandidat=<?= $candidat->numCandidat ?>"> Retour </a>

