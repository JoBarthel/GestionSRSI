<h1> Informations de <?= $candidat->getNomComplet() ?></h1>

    <div class='doc'>
    Mail : <a class ="mail" href="mailto:<?= $candidat->mail ?>"> <?= $candidat->mail ?> </a></div>
    Téléphone : <?= $candidat->telephone ?> </br>
    Adresse : <?= $candidat->adresse ?> </br>
    Date de naissance : <?= $candidat->dateNaissance ?> </br>
    Année du bac : <?= $candidat->anneeBac ?></br>
    Statut : <?= $candidat->getStatut()->nomStatut ?>
    <a class='button' href="modifierStatut.php?numCandidat=<?=$candidat->numCandidat?>"> Modifier le statut </a> </br>

<h2 class="titre">
    Ses documents à valider
</h2>
    <?php
    //la liste des documents valides
    $listeDocTemp = [];

    //parcours la liste des documents de ce candidat
    for ($i = 0; $i < count($listeDoc); $i++) {

        $doc = new Document($listeDoc[$i]);

        //si pas encore valide ou  refusé, alors l'afficher
        if($doc->getStatut()->numStatut != StatutDoc::numStatutValideActuel() && $doc->getStatut()->numStatut != StatutDoc::numStatutRefuseActuel())
            echo "<div class='doc'> ".$doc->telecharger()."</div> 
           <div class ='button'> <a href='actions/validerDocument.php?numDoc=".$doc->numDoc."&numCandidat=".$candidat->numCandidat."'> Valider </a> </br> </div>
             <a class=\"button\" href='refuserDocument.php?numDoc=".$doc->numDoc."&numCandidat=".$candidat->numCandidat."'> Refuser </a> ";

        //Si valide, stocker dans un tableau temporaire
        else if($doc->getStatut()->numStatut == StatutDoc::numStatutValideActuel()) $listeDocTemp[count($listeDocTemp)] = $doc;
    }
    ?>



<h2 class="titre">
    Ses documents validés
</h2>
    <?php

    //parcours la liste temporaire des documents validés
    for($i = 0 ; $i < count($listeDocTemp) ; $i++)
    {
        echo "<div class='doc'> ".$listeDocTemp[$i]->telecharger()."</div>";
    }
    ?>


<h2 class="titre">
    Ses réponses aux offres
</h2>
    <h3> Offres acceptées</h3>
    <?php
        foreach ($listeOffresAcceptees as $offre)
        {
            echo "<p><div class='doc'><a href='offre.php?offre=".$offre->numOffre."'>".$offre->societe.": ".$offre->poste." </a></p> </div>   ";
        }
    ?>
    <h3> Offres refusées </h3>
    <?php
        for($i = 0 ; $i < count($listeOffresRefusees) ; $i++)
        {
            $offre = $listeOffresRefusees[$i];
            $raison = $listeRaisonsRefus[$i];

            echo "<div class='doc'><p></p><a href='offre.php?offre=".$offre->numOffre."'>".$offre->societe.": ".$offre->poste." </a>. Raison : ".$raison."</p></div>";
        }
    ?>

    </br>
    <h3>
        <div class='doc'><a href = "fiches.php?numCandidat=<?=$candidat->numCandidat?>"> Son formulaire de suivi </a> </div>
    </h3>


    <h3>
        <div class='doc'><a href = "historiqueCandidat.php?numCandidat=<?=$candidat->numCandidat?>"> Son historique </a> </div>
    </h3>

<a class="button" data-confirm="Êtes-vous sûr de vouloir supprimer ce candidat ?" href="actions/supprimerCandidat.php?numCandidat=<?=$candidat->numCandidat?>"> Supprimer ce candidat </a>