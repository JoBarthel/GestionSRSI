<div class="contactRecruteur">
    <h3>Contact Recruteur</h3>
    <p>	Nom:
        <?php echo $offre->nomResponsable; ?>
        </br>
        Mail:
        <?php echo $offre->mailResponsable; ?>
        </br>
        Téléphone:
        <?php echo $offre->telResponsable; ?>
    </p>
    </table>
</div>

<div class="descriptionOffre">
    <h2>Nom de l'offre</h2>
    <p>Poste: <?php echo $offre->poste; ?>
        </br>
        Entreprise: <?php echo $offre->societe; ?>
        </br>
        Description de l'offre : <?php echo $offre->description; ?></p>
</div>

<div class="fiche">

    <h2>Relances</h2>
    <p>

        <?php
        //affiche toutes les relances de la fiche
        if(!empty($listeRelances)){

            foreach ($listeRelances as $relance)
                echo "".$relance->dateRelance.": ".$relance->typeRelance."</br>";

        }
        else echo("<p>Pas de relances</p></br>");

        ?>
    </p>

    <h2>Remarques</h2>

    <?php  echo $fiche->remarque; ?>

<div class="fiche">
    <h2>Entretiens</h2>

    <?php
    if(count($listeEntretien) == 0)echo "Il n'y a pas encore eu d'entretiens";
    for ($i = 0; $i < count($listeEntretien); $i++) {
        $entretien = $listeEntretien[$i];
        echo "<h3>Entretien du ".$entretien->dateEntretien."</h3></br><p>".$entretien->resumeEntretien."</p></br>";
    }
    echo '<a class="button" href="fiches.php?numCandidat='.$candidat->numCandidat.'"> Retour </a> ';
    ?>
</div>