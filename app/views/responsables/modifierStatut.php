<h1> Modifier le statut de <?= $candidat->getNomComplet() ?></h1>

<h2> Statut actuel : <?= $candidat->getStatut()->nomStatut ?></h2>

<!-- Liste des statuts dispo -->
<h3> Nouveau statut </h3>
<form method="post" action="actions/modifierStatut.php">
    <input type="hidden" name="numCandidat" value="<?= $candidat->numCandidat ?>">
    <select name="statut">
        <?php
        foreach ($listeStatut as $s) {
            $statut = new Statut($s);
            echo '<option value=' . $statut->numStatut . '> ' . $statut->nomStatut . '</option>';
        }
        ?>
    </select>
    <input type="submit" value="Modifier le statut">
</form>

<!--Formulaire de nouveau statut -->
<h2> Ajouter un statut</h2>
<h3> Utilisez ce formulaire si aucun statut de la liste çi-dessus ne vous convient</h3>
<form method="post" action="actions/ajouterStatut.php">
    <p> Nom du statut <input type="text" name="nomStatut"/></p>
    <p><textarea name="descStatut" rows="10" cols="40" maxlength="100" placeholder="Description du statut ici.. "></textarea></p>
    <input type="hidden" name="numCandidat" value="<?= $candidat->numCandidat?>"/>
    <p><input type="submit" value="Ajouter un statut"></p>
</form>

