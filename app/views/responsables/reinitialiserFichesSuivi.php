
<!-- Affiche la date de la prochaine réinitialisation -->
<h3> La prochaine réinitialisation est prévu pour le <?=Candidat::prochaineInitialisation()?></h3>

<!-- Bouton réinitialiser -->
<a class="button" href = 'actions/reinitialiserFichesSuivi.php'> Réinitialiser la validation des fiches de suivi </a>

<p> Le statut des fiches de suivi de tout les candidats sera remis à "En attente de validation" </p>