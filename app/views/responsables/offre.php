<h1>Offre du <?= $offre->dateOffre ?></h1>
<h2><?= $offre->poste ?></h2>
<h3 class="titre">Société</h3>
<h3><?= $offre->societe ?></h3>
<p><?= $offre->adresse ?></p>
<h3 class="titre">Description de l'offre</h3>
<p><?= $offre->description ?></p>
<h3 class="titre">Contact</h3>
<p>Nom: <?= $offre->nomResponsable ?></p>
<p>Mail: <?= $offre->mailResponsable ?></p>
<p>Téléphone: <?= $offre->telResponsable ?></p>

<h3 class="titre"> Documents liés </h3>
    <?php
        foreach ($listeDocs as $doc) echo $doc->telecharger();
    ?>
<h3 class="titre"> Instructions de candidature</h3>
<p><?= $offre->instructionsPostule ?>

<?php

?>
