<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/candidats.css"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title><?= !empty($title) ? $title. ' - ' : '' ?>Espace candidat</title>
</head>
<body>

<div class="bandeau">
    <div class="deconnexion"><a href="deconnexion.php">Déconnexion</a></div>
    <h1><?= !empty($title) ? $title : 'Espace candidat' ?></h1>
</div>
<div class="flash">
    <?php foreach(flash() as $message) { echo "<p>$message</p>"; } ?>
</div>

<div class="page">
    <div class="menu">
        <ul class="deroulant">

            <li><a href="index.php">Offres d'Embauches</a></li>
            <li><a href="infosPubliques.php"> Informations publiques </a></li>
            <li><a href="informations.php">Mes informations personnelles</a></li>
            <li >
                Mes recherches d'entreprise
                <ul>
                    <li><a href="fiches.php">Mes fiches de suivi</a></li>
                    <li><a href="documents.php">Mes documents</a></li>
                </ul>
            </li>
            <li><a href="contact.php">Contacts Responsables</a></li>
        </ul>
    </div>
    <div class="contenu">
        <?= $this->contents ?>
    </div>
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>