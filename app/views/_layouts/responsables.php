<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/responsables.css"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title><?= !empty($title) ? $title. ' - ' : '' ?>Espace responsable</title>
</head>
<body>

<div class="bandeau">
    <div class="deconnexion"><a href="deconnexion.php">Déconnexion</a></div>
    <h1><?= !empty($title) ? $title : 'Espace responsable' ?></h1>
    <p> <!-- Afficher Nom Prenom Etudiant --></p>
</div>
<div class="flash">
    <?php foreach(flash() as $message) { echo "<p>$message</p>"; } ?>
   
</div>

<div class="page">
    <div class="menu">
        <ul>
            <!-- <li><a href="index.php">Accueil</a></li> -->
            <li><a href="infosPubliques.php"> Informations publiques </a></li>
            <li><a href="ajout.php">Ajout de candidat/responsable</a></li>
            <li><a href="contact.php">Les responsables </a></li>
            <li><a href="listeCandidats.php">Les candidats</a></li>
            <li><a href="offresEmbauche.php">Les offres d'embauche</a></li>
            <li><a href="reinitialiserFichesSuivi.php"> Réinitialiser la validation des fiches de suivi </a></li>
            <li><a href="configurerAlertesMail.php"> Configurer les alertes mail </a></li>
            <li><a href="changerMotDePasse.php"> Modifier mon mot de passe </a></li>
        </ul>
    </div>
    <div class="contenu">
        <?= $this->contents ?>
    </div>
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>