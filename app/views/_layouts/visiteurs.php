<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/visiteurs.css"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?= $title ?></title>
</head>
<body>
<div class="bandeau">
    <h1><?= $bigtitle ?></h1>
</div>
<div class="flash">
    <?php foreach(flash() as $message) { echo "<p>$message</p>"; } ?>
</div>
<div class="main">
    <?= $this->contents ?>
</div>
</body>
</html>