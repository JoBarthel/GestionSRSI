<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/main.css">
	<title>Erreur <?= $code ?></title>
</head>
<body>
<div class="bandeau">
    <h1>Erreur <?= $code ?></h1>
</div>
<p class="_error"><?= $message ?></p>
</body>
</html>