<?php

require 'app/bootstrap.php';

//Authentification requise
if (!Auth::logged()) redirect('index.php');

$offre = new OffreEmbauche($_GET['offre']);

//Si candidat on peut voir les entretiens et les modifier
if (Auth::user()->estCandidat()) {


    $listeEntretien = (new ReponseOffre(Utilisateur::get(Auth::user()->numUtilisateur)->numCandidat, $_GET['offre']))->toutSesEntretiens();
    $user= Auth::user();

    //fiche de suivi
    $fiche = new FicheDeSuivi($offre->numOffre, $user->numCandidat);

    //relances de cette fiche
    $listeRelances = $fiche->toutesSesRelances();

    //est ce qu'on est en mode modification de remarque ?
    if(isset($_GET['modifierRemarque'])) $modifierRemarque = $_GET['modifierRemarque'];
    else $modifierRemarque = false;

    $layout = new Layout('candidats');
    include view('candidats/ficheEnt.php');
    $layout->show('Suivi des Entretiens');

}

//Si responsable on peut voir les entretiens
else if(Auth::user()->estResponsable()){

    $candidat = new Candidat($_GET['candidat']);
    $listeEntretien = (new ReponseOffre($candidat->numCandidat, $_GET['offre']))->toutSesEntretiens();

    //fiche de suivi
    $fiche = new FicheDeSuivi($offre->numOffre, $candidat->numCandidat);

    //relances de cette fiche
    $listeRelances = $fiche->toutesSesRelances();

    $layout = new Layout('responsables');
    include view('responsables/ficheEnt.php');
    $layout->show('Suivi des Entretiens');

}

