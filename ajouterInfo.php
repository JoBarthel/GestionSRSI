<?php
/**
 * Created by PhpStorm.
 * User: Célia
 * Date: 27/12/2016
 * Time: 23:04
 */

require 'app/bootstrap.php';

// Authentification requise
if (!Auth::logged()) redirect('index.php');

//etre un responsable requis
if (Auth::user()->estResponsable()) {
    $layout = new Layout('responsables');
    include view('responsables/ajouterInfo.php');
    $layout->show('Ajouter une information');
} else redirect('index.php');
