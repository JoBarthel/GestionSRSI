<?php

require 'app/bootstrap.php';

// Authentification requise
if (!Auth::logged()) redirect('index.php');
//Si candidat, alors afficher la fiche de suivi et permettre de la modifier
if (Auth::user()->estCandidat()) {


    $candidat = Utilisateur::get(Auth::user()->numUtilisateur);
    $listeOffre = $candidat->toutesSesReponsesAcceptes();

    $layout = new Layout('candidats');
    include view('candidats/fiches.php');
    $layout->show('Mes Fiches');
}
//Si responsable, alors afficher la fiche de suivi et permettre de la valider
else if(Auth::user()->estResponsable()) {
    $candidat = new Candidat($_GET['numCandidat']);
    $listeOffre = $candidat->toutesSesReponsesAcceptes();
    $listeRaisonRefus = RaisonRefusSuivi::lister();

    $layout = new Layout('responsables');
    include view('responsables/fiches.php');
    $layout->show('Fiches de '.$candidat->getNomComplet());
}