-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 04 Janvier 2017 à 17:02
-- Version du serveur :  10.1.19-MariaDB
-- Version de PHP :  7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `candidat`
--

CREATE TABLE `candidat` (
  `numCandidat` int(11) NOT NULL,
  `dateNaissance` date DEFAULT NULL,
  `telephone` varchar(10) DEFAULT NULL,
  `adresse` varchar(250) DEFAULT NULL,
  `anneeBac` varchar(25) DEFAULT NULL,
  `diplomeBacPlus2` varchar(25) DEFAULT NULL,
  `entrepriseActuelle` varchar(25) DEFAULT NULL,
  `numUtilisateur` int(11) DEFAULT NULL,
  `numStatut` int(11) DEFAULT NULL,
  `statutFicheSuivi` int(11) NOT NULL,
  `numRaison` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `candidat`
--

INSERT INTO `candidat` (`numCandidat`, `dateNaissance`, `telephone`, `adresse`, `anneeBac`, `diplomeBacPlus2`, `entrepriseActuelle`, `numUtilisateur`, `numStatut`, `statutFicheSuivi`, `numRaison`) VALUES
(1, '1997-11-25', '0786938031', '7 rue auguste demmler', '2014', 'DUT Informatique', NULL, 143, 10, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `datereinitialisation`
--

CREATE TABLE `datereinitialisation` (
  `dateProchaineReinitialisation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `datereinitialisation`
--

INSERT INTO `datereinitialisation` (`dateProchaineReinitialisation`) VALUES
('2017-01-11');

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE `document` (
  `numDoc` int(11) NOT NULL,
  `nomDoc` varchar(50) DEFAULT NULL,
  `numStatut` int(11) DEFAULT '3',
  `numOffre` int(11) DEFAULT NULL,
  `dateMod` date DEFAULT NULL,
  `chemin` varchar(200) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `numUtilisateur` int(11) DEFAULT NULL,
  `numReponseOffre` int(11) DEFAULT NULL,
  `public` tinyint(1) DEFAULT NULL,
  `numInfo` int(11) DEFAULT NULL,
  `numRaison` int(11) DEFAULT NULL,
  `emplacementDoc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `document`
--

INSERT INTO `document` (`numDoc`, `nomDoc`, `numStatut`, `numOffre`, `dateMod`, `chemin`, `type`, `numUtilisateur`, `numReponseOffre`, `public`, `numInfo`, `numRaison`, `emplacementDoc`) VALUES
(13, 'Fiche de poste.txt', 3, 12, '2017-01-04', 'C:\\xampp\\htdocs\\projet_FINAL\\actions/../documents/admin5be5e5a5dce5954fa5b90f87b77f96233205eb59.txt', NULL, 142, NULL, 0, NULL, NULL, 'admin5be5e5a5dce5954fa5b90f87b77f96233205eb59'),
(14, 'Lettre de motivation.txt', 3, NULL, '2017-01-04', 'C:\\xampp\\htdocs\\projet_FINAL/documents/APAVOU898e3f5addcbbec77a534fb5d2de87ed2d9b043c.txt', 2, 143, NULL, 0, NULL, NULL, 'APAVOU898e3f5addcbbec77a534fb5d2de87ed2d9b043c'),
(15, 'CV.docx', 3, NULL, '2017-01-04', 'C:\\xampp\\htdocs\\projet_FINAL/documents/APAVOU29de5d1788593663b831c5ac07e87b32c0d79e3d.docx', 1, 143, NULL, 0, NULL, NULL, 'APAVOU29de5d1788593663b831c5ac07e87b32c0d79e3d');

-- --------------------------------------------------------

--
-- Structure de la table `documentreponseoffre`
--

CREATE TABLE `documentreponseoffre` (
  `numDoc` int(11) NOT NULL,
  `numReponseOffre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `documentreponseoffre`
--

INSERT INTO `documentreponseoffre` (`numDoc`, `numReponseOffre`) VALUES
(14, 5),
(15, 5);

-- --------------------------------------------------------

--
-- Structure de la table `entretien`
--

CREATE TABLE `entretien` (
  `numEntretien` int(11) NOT NULL,
  `dateEntretien` date NOT NULL,
  `resumeEntretien` varchar(100) NOT NULL,
  `numOffre` int(11) NOT NULL,
  `numCandidat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `entretien`
--

INSERT INTO `entretien` (`numEntretien`, `dateEntretien`, `resumeEntretien`, `numOffre`, `numCandidat`) VALUES
(4, '2017-01-04', 'L''entretien s''est bien passé.', 12, 1);

-- --------------------------------------------------------

--
-- Structure de la table `fichesuivi`
--

CREATE TABLE `fichesuivi` (
  `numFicheSuivi` int(11) NOT NULL,
  `numOffre` int(11) NOT NULL,
  `numCandidat` int(11) NOT NULL,
  `remarque` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `fichesuivi`
--

INSERT INTO `fichesuivi` (`numFicheSuivi`, `numOffre`, `numCandidat`, `remarque`) VALUES
(4, 12, 1, 'Cette offre a de bonnes chances de déboucher sur une embauche.');

-- --------------------------------------------------------

--
-- Structure de la table `historiquecandidat`
--

CREATE TABLE `historiquecandidat` (
  `numHistorique` int(11) NOT NULL,
  `numCandidat` int(11) NOT NULL,
  `dateValidation` date NOT NULL,
  `numStatut` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `historiquecandidat`
--

INSERT INTO `historiquecandidat` (`numHistorique`, `numCandidat`, `dateValidation`, `numStatut`) VALUES
(104, 1, '2017-01-04', 2);

-- --------------------------------------------------------

--
-- Structure de la table `infopublique`
--

CREATE TABLE `infopublique` (
  `numInfo` int(11) NOT NULL,
  `dateInfo` date NOT NULL,
  `texteInfo` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `infopublique`
--

INSERT INTO `infopublique` (`numInfo`, `dateInfo`, `texteInfo`) VALUES
(1, '2017-01-02', 'Bienvenue sur la plateforme de gestion des candidatures License Pro SRSI !'),
(2, '2017-01-04', 'Ici se déroulera la procédure de recrutement.\r\n');

-- --------------------------------------------------------

--
-- Structure de la table `offreembauche`
--

CREATE TABLE `offreembauche` (
  `numOffre` int(11) NOT NULL,
  `poste` varchar(40) DEFAULT NULL,
  `societe` varchar(25) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `nomResponsable` varchar(30) DEFAULT NULL,
  `mailResponsable` varchar(40) DEFAULT NULL,
  `telResponsable` varchar(10) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `documentsaFounir` varchar(200) DEFAULT NULL,
  `dateOffre` date DEFAULT NULL,
  `instructionsPostule` varchar(200) NOT NULL DEFAULT 'Pas d''instructions particulières'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `offreembauche`
--

INSERT INTO `offreembauche` (`numOffre`, `poste`, `societe`, `adresse`, `nomResponsable`, `mailResponsable`, `telResponsable`, `description`, `documentsaFounir`, `dateOffre`, `instructionsPostule`) VALUES
(3, 'Nom du Poste', 'Société Test', '235 Avenue des Carillions', 'Eric Jotu', 'eric.jotu@contact.fr', '0696857412', 'Travailler sur de nombreuses tâches en équipe.', NULL, '2017-01-04', 'CV et Lettre de Motivation'),
(12, 'Administrateur Réseau', 'Société One', '7 Rue Auguste Demmler', 'Charles Martin', 'charles.martin@gmail.com', '9699876543', ' Configurer le réseau de l''entreprise et l''entretenir. ', NULL, '2017-01-04', 'Présentez CV et lettre de motivation en .pdf.');

-- --------------------------------------------------------

--
-- Structure de la table `raisonrefus`
--

CREATE TABLE `raisonrefus` (
  `numRaison` int(11) NOT NULL,
  `nomRaison` varchar(25) NOT NULL,
  `descRaison` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `raisonrefus`
--

INSERT INTO `raisonrefus` (`numRaison`, `nomRaison`, `descRaison`) VALUES
(1, 'Fautes d''orthographe', 'L''orthographe de ce document est inacceptable.'),
(2, 'Présentation non conforme', 'La présentation de ce document n''est pas conforme aux standards donnés. ');

-- --------------------------------------------------------

--
-- Structure de la table `raisonrefussuivi`
--

CREATE TABLE `raisonrefussuivi` (
  `numRaison` int(11) NOT NULL,
  `nomRaison` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `raisonrefussuivi`
--

INSERT INTO `raisonrefussuivi` (`numRaison`, `nomRaison`) VALUES
(1, 'Pas d''entretiens'),
(2, 'Pas de nouvelles');

-- --------------------------------------------------------

--
-- Structure de la table `relance`
--

CREATE TABLE `relance` (
  `numRelance` int(11) NOT NULL,
  `dateRelance` date NOT NULL,
  `typeRelance` varchar(25) NOT NULL,
  `numFicheSuivi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `relance`
--

INSERT INTO `relance` (`numRelance`, `dateRelance`, `typeRelance`, `numFicheSuivi`) VALUES
(7, '2017-01-04', 'Téléphone', 4),
(8, '2017-01-01', 'Mail', 4);

-- --------------------------------------------------------

--
-- Structure de la table `reponseoffre`
--

CREATE TABLE `reponseoffre` (
  `numReponseOffre` int(11) NOT NULL,
  `numCandidat` int(11) NOT NULL,
  `numOffre` int(11) NOT NULL,
  `accepte` tinyint(1) DEFAULT NULL,
  `motifRefus` varchar(100) DEFAULT NULL,
  `typedoc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `reponseoffre`
--

INSERT INTO `reponseoffre` (`numReponseOffre`, `numCandidat`, `numOffre`, `accepte`, `motifRefus`, `typedoc`) VALUES
(1, 1, 3, 0, 'Pas de documents', 0),
(5, 1, 12, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `responsable`
--

CREATE TABLE `responsable` (
  `numResponsable` int(11) NOT NULL,
  `numUtilisateur` int(11) DEFAULT NULL,
  `alerteSurReset` tinyint(1) NOT NULL DEFAULT '0',
  `alerteSurNouveauCandidat` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `responsable`
--

INSERT INTO `responsable` (`numResponsable`, `numUtilisateur`, `alerteSurReset`, `alerteSurNouveauCandidat`) VALUES
(14, 142, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

CREATE TABLE `statut` (
  `numStatut` int(11) NOT NULL,
  `nomStatut` varchar(25) NOT NULL,
  `descStatut` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `statut`
--

INSERT INTO `statut` (`numStatut`, `nomStatut`, `descStatut`) VALUES
(8, 'Contrat signé', 'Ce candidat a signé un contrat avec son entreprise'),
(9, 'Promesse d''embauche', 'Ce candidat a été embauché et signera son contrat sous peu. '),
(10, 'En recherche d''entreprise', 'Ce candidat cherche encore son entreprise. ');

-- --------------------------------------------------------

--
-- Structure de la table `statutdoc`
--

CREATE TABLE `statutdoc` (
  `numStatut` int(11) NOT NULL,
  `nomStatut` varchar(25) NOT NULL,
  `descStatut` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `statutdoc`
--

INSERT INTO `statutdoc` (`numStatut`, `nomStatut`, `descStatut`) VALUES
(1, 'Validé', 'Le statut a été vérifié par un responsable. '),
(3, 'En cours de validation', 'Le document n''a pas encore été passé en revue par un responsable. '),
(4, 'Non validé', 'Le document est non conforme.');

-- --------------------------------------------------------

--
-- Structure de la table `statutfichesuivi`
--

CREATE TABLE `statutfichesuivi` (
  `numStatut` int(11) NOT NULL,
  `nomStatut` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `statutfichesuivi`
--

INSERT INTO `statutfichesuivi` (`numStatut`, `nomStatut`) VALUES
(1, 'en attente de validation'),
(2, 'validée'),
(3, 'refusée');

-- --------------------------------------------------------

--
-- Structure de la table `typedoc`
--

CREATE TABLE `typedoc` (
  `numType` int(11) NOT NULL,
  `nomType` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `typedoc`
--

INSERT INTO `typedoc` (`numType`, `nomType`) VALUES
(1, 'CV'),
(2, 'Lettre de motivation'),
(3, 'Fiche Entreprise'),
(4, 'Autre'),
(5, 'Lettre de recommandation'),
(6, 'Relevé de notes');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `numUtilisateur` int(11) NOT NULL,
  `login` varchar(250) DEFAULT NULL,
  `motDePasse` varchar(255) DEFAULT NULL,
  `numCandidat` int(11) DEFAULT NULL,
  `numResponsable` int(11) DEFAULT NULL,
  `nom` varchar(25) DEFAULT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `mail` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`numUtilisateur`, `login`, `motDePasse`, `numCandidat`, `numResponsable`, `nom`, `prenom`, `mail`) VALUES
(142, 'admin.admin', '$2y$10$Ui2hTMlzyyhJwlqWYpqFX.AHpUCpjMMNfk0bhRBCHKt7RHoQ4Xu6y', NULL, 14, 'admin', 'admin', 'josephine.barthel@free.fr'),
(143, 'florian.apavou', '$2y$10$FZWaqtEx4FyqhBjUxxqXwe4AgAoBC7t7pQXiS77qvyIeHN8kcCjii', 1, NULL, 'APAVOU', 'Florian', 'florian.apavou@u-psud.fr');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `candidat`
--
ALTER TABLE `candidat`
  ADD PRIMARY KEY (`numCandidat`),
  ADD KEY `FK_Candidat_numUtilisateur` (`numUtilisateur`),
  ADD KEY `fk_candidat_numStatut` (`numStatut`),
  ADD KEY `numRaison` (`numRaison`),
  ADD KEY `statutFicheSuivi` (`statutFicheSuivi`);

--
-- Index pour la table `datereinitialisation`
--
ALTER TABLE `datereinitialisation`
  ADD PRIMARY KEY (`dateProchaineReinitialisation`);

--
-- Index pour la table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`numDoc`),
  ADD KEY `FK_Document_numUtilisateur` (`numUtilisateur`),
  ADD KEY `FK_Document_numreponseOffre` (`numReponseOffre`),
  ADD KEY `numStatut` (`numStatut`),
  ADD KEY `numRaison` (`numRaison`),
  ADD KEY `numOffre` (`numOffre`),
  ADD KEY `numInfo` (`numInfo`),
  ADD KEY `type` (`type`);

--
-- Index pour la table `documentreponseoffre`
--
ALTER TABLE `documentreponseoffre`
  ADD KEY `numDoc` (`numDoc`),
  ADD KEY `numReponseOffre` (`numReponseOffre`);

--
-- Index pour la table `entretien`
--
ALTER TABLE `entretien`
  ADD PRIMARY KEY (`numEntretien`),
  ADD KEY `numFiche` (`numOffre`),
  ADD KEY `numCandidat` (`numCandidat`);

--
-- Index pour la table `fichesuivi`
--
ALTER TABLE `fichesuivi`
  ADD PRIMARY KEY (`numFicheSuivi`),
  ADD KEY `numOffre` (`numOffre`),
  ADD KEY `numCandidat` (`numCandidat`);

--
-- Index pour la table `historiquecandidat`
--
ALTER TABLE `historiquecandidat`
  ADD PRIMARY KEY (`numHistorique`),
  ADD KEY `numCandidat` (`numCandidat`),
  ADD KEY `numStatut` (`numStatut`);

--
-- Index pour la table `infopublique`
--
ALTER TABLE `infopublique`
  ADD PRIMARY KEY (`numInfo`);

--
-- Index pour la table `offreembauche`
--
ALTER TABLE `offreembauche`
  ADD PRIMARY KEY (`numOffre`);

--
-- Index pour la table `raisonrefus`
--
ALTER TABLE `raisonrefus`
  ADD PRIMARY KEY (`numRaison`);

--
-- Index pour la table `raisonrefussuivi`
--
ALTER TABLE `raisonrefussuivi`
  ADD PRIMARY KEY (`numRaison`);

--
-- Index pour la table `relance`
--
ALTER TABLE `relance`
  ADD PRIMARY KEY (`numRelance`),
  ADD KEY `numFicheSuivi` (`numFicheSuivi`);

--
-- Index pour la table `reponseoffre`
--
ALTER TABLE `reponseoffre`
  ADD PRIMARY KEY (`numReponseOffre`),
  ADD UNIQUE KEY `numReponseOffre` (`numReponseOffre`),
  ADD KEY `FK_reponseOffre_numCandidat` (`numCandidat`),
  ADD KEY `FK_reponseOffre_numOffre` (`numOffre`);

--
-- Index pour la table `responsable`
--
ALTER TABLE `responsable`
  ADD PRIMARY KEY (`numResponsable`),
  ADD KEY `FK_Responsable_numUtilisateur` (`numUtilisateur`);

--
-- Index pour la table `statut`
--
ALTER TABLE `statut`
  ADD PRIMARY KEY (`numStatut`);

--
-- Index pour la table `statutdoc`
--
ALTER TABLE `statutdoc`
  ADD PRIMARY KEY (`numStatut`);

--
-- Index pour la table `statutfichesuivi`
--
ALTER TABLE `statutfichesuivi`
  ADD PRIMARY KEY (`numStatut`);

--
-- Index pour la table `typedoc`
--
ALTER TABLE `typedoc`
  ADD PRIMARY KEY (`numType`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`numUtilisateur`),
  ADD KEY `FK_Utilisateur_numCandidat` (`numCandidat`),
  ADD KEY `FK_Utilisateur_numResponsable` (`numResponsable`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `candidat`
--
ALTER TABLE `candidat`
  MODIFY `numCandidat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `document`
--
ALTER TABLE `document`
  MODIFY `numDoc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `entretien`
--
ALTER TABLE `entretien`
  MODIFY `numEntretien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `fichesuivi`
--
ALTER TABLE `fichesuivi`
  MODIFY `numFicheSuivi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `historiquecandidat`
--
ALTER TABLE `historiquecandidat`
  MODIFY `numHistorique` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT pour la table `infopublique`
--
ALTER TABLE `infopublique`
  MODIFY `numInfo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `offreembauche`
--
ALTER TABLE `offreembauche`
  MODIFY `numOffre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `raisonrefus`
--
ALTER TABLE `raisonrefus`
  MODIFY `numRaison` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `raisonrefussuivi`
--
ALTER TABLE `raisonrefussuivi`
  MODIFY `numRaison` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `relance`
--
ALTER TABLE `relance`
  MODIFY `numRelance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `reponseoffre`
--
ALTER TABLE `reponseoffre`
  MODIFY `numReponseOffre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `responsable`
--
ALTER TABLE `responsable`
  MODIFY `numResponsable` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `statut`
--
ALTER TABLE `statut`
  MODIFY `numStatut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `statutdoc`
--
ALTER TABLE `statutdoc`
  MODIFY `numStatut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `statutfichesuivi`
--
ALTER TABLE `statutfichesuivi`
  MODIFY `numStatut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `typedoc`
--
ALTER TABLE `typedoc`
  MODIFY `numType` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `numUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `candidat`
--
ALTER TABLE `candidat`
  ADD CONSTRAINT `candidat_ibfk_1` FOREIGN KEY (`statutFicheSuivi`) REFERENCES `statutfichesuivi` (`numStatut`) ON DELETE CASCADE,
  ADD CONSTRAINT `candidat_ibfk_2` FOREIGN KEY (`numRaison`) REFERENCES `raisonrefussuivi` (`numRaison`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_candidat_numStatut` FOREIGN KEY (`numStatut`) REFERENCES `statut` (`numStatut`),
  ADD CONSTRAINT `fk_candidat_numUtilisateur` FOREIGN KEY (`numUtilisateur`) REFERENCES `utilisateur` (`numUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `DocDeleteCandidat` FOREIGN KEY (`numUtilisateur`) REFERENCES `utilisateur` (`numUtilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `document_ibfk_1` FOREIGN KEY (`numRaison`) REFERENCES `raisonrefus` (`numRaison`) ON DELETE CASCADE,
  ADD CONSTRAINT `document_ibfk_2` FOREIGN KEY (`numReponseOffre`) REFERENCES `reponseoffre` (`numReponseOffre`) ON DELETE CASCADE,
  ADD CONSTRAINT `document_ibfk_3` FOREIGN KEY (`numInfo`) REFERENCES `infopublique` (`numInfo`) ON DELETE CASCADE,
  ADD CONSTRAINT `document_ibfk_4` FOREIGN KEY (`type`) REFERENCES `typedoc` (`numType`),
  ADD CONSTRAINT `document_ibfk_5` FOREIGN KEY (`numStatut`) REFERENCES `statutdoc` (`numStatut`) ON DELETE CASCADE;

--
-- Contraintes pour la table `documentreponseoffre`
--
ALTER TABLE `documentreponseoffre`
  ADD CONSTRAINT `documentreponseoffre_ibfk_1` FOREIGN KEY (`numDoc`) REFERENCES `document` (`numDoc`) ON DELETE CASCADE,
  ADD CONSTRAINT `documentreponseoffre_ibfk_2` FOREIGN KEY (`numReponseOffre`) REFERENCES `reponseoffre` (`numReponseOffre`) ON DELETE CASCADE;

--
-- Contraintes pour la table `entretien`
--
ALTER TABLE `entretien`
  ADD CONSTRAINT `EntretienDeleteCandidat` FOREIGN KEY (`numCandidat`) REFERENCES `candidat` (`numCandidat`) ON DELETE CASCADE,
  ADD CONSTRAINT `EntretienDeleteOffre` FOREIGN KEY (`numOffre`) REFERENCES `offreembauche` (`numOffre`) ON DELETE CASCADE;

--
-- Contraintes pour la table `fichesuivi`
--
ALTER TABLE `fichesuivi`
  ADD CONSTRAINT `FicheDeleteCandidat` FOREIGN KEY (`numCandidat`) REFERENCES `candidat` (`numCandidat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FicheDeleteOffre` FOREIGN KEY (`numOffre`) REFERENCES `offreembauche` (`numOffre`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `historiquecandidat`
--
ALTER TABLE `historiquecandidat`
  ADD CONSTRAINT `historiquecandidat_ibfk_1` FOREIGN KEY (`numStatut`) REFERENCES `statutfichesuivi` (`numStatut`) ON DELETE CASCADE,
  ADD CONSTRAINT `historiquecandidat_ibfk_2` FOREIGN KEY (`numCandidat`) REFERENCES `candidat` (`numCandidat`) ON DELETE CASCADE;

--
-- Contraintes pour la table `relance`
--
ALTER TABLE `relance`
  ADD CONSTRAINT `relance_ibfk_1` FOREIGN KEY (`numFicheSuivi`) REFERENCES `fichesuivi` (`numFicheSuivi`) ON DELETE CASCADE;

--
-- Contraintes pour la table `reponseoffre`
--
ALTER TABLE `reponseoffre`
  ADD CONSTRAINT `fk_reponseOffre_numCandidat` FOREIGN KEY (`numCandidat`) REFERENCES `candidat` (`numCandidat`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_reponseOffre_numOffre` FOREIGN KEY (`numOffre`) REFERENCES `offreembauche` (`numOffre`) ON DELETE CASCADE;

--
-- Contraintes pour la table `responsable`
--
ALTER TABLE `responsable`
  ADD CONSTRAINT `responsable_ibfk_1` FOREIGN KEY (`numUtilisateur`) REFERENCES `utilisateur` (`numUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `UserDeleteCandidat` FOREIGN KEY (`numCandidat`) REFERENCES `candidat` (`numCandidat`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
