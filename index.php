<?php

require 'app/bootstrap.php';

/**
 * La page index.php est particulière dans la mesure où
 * son contenu s'adapte à l'utilisateur. Elle est autant
 * accessible par les visiteurs que les utilisateurs connectés
 */

if(Auth::logged()) // si connecté
{
    $utilisateur = Auth::user();

    if($utilisateur->estCandidat()) // si candidat
    {
        /*if($utilisateur->numStatut != 10) { 
            redirect('infosPubliques.php');
        }
        else{*/
            $layout = new Layout('candidats');
            include view('candidats/accueil.php');
            $layout->show('Accueil');
        //} 
    }
    else // si responsable
    {
        $listeInfos = InfoPublique::listerInfos();
        $layout = new Layout('responsables');
        include view('responsables/infosPubliques.php');
        $layout->show('Accueil');
    }
}
else // si visiteur
{
    $layout = new Layout('visiteurs');
    include view('visiteurs/connexion.php');
    $layout->show('Connexion - Licence SRSI', 'Bienvenue !');
}