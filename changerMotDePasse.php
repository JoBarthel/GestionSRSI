<?php

require 'app/bootstrap.php';

//Authentification requise
if (!Auth::logged()) redirect('index.php');

//si responsable on peut changer son propre mot de passe
if (Auth::user()->estResponsable()) {

    $user = Auth::user();

    $layout = new Layout('responsables');
    include view('responsables/changerMotDePasse.php');
    $layout->show('Changer mon mot de passe');
} else redirect("index.php");