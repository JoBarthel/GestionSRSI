<?php

require 'app/bootstrap.php';

if (!Auth::logged() || !Auth::user()->estCandidat()) redirect('index.php');

$user = Auth::user();
$offre=$_GET['offre'];
$layout = new Layout('candidats');
include view('candidats/relanceAjout.php');
$layout->show('Ajout d\'une Relance');

?>