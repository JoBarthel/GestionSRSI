<?php

require 'app/bootstrap.php';

// Authentification requise
if (!Auth::logged()) redirect('index.php');

//etre un candidat requis
if (!Auth::user()->estCandidat()) error(403);

$tabDoc=$_POST['docs'];
$numOffre = $_GET['offre'];
$reponseOffre = ReponseOffre::ajouteReponseOffre(Auth::user()->numCandidat,$numOffre,1);
$fiche = FicheDeSuivi::ajouteFicheDeSuivi($reponseOffre->numOffre,Auth::user()->numCandidat);

foreach($tabDoc as $numDoc){
    $reponseOffre->ajouterDoc($numDoc);
}
redirect('index.php');