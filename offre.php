<?php

require 'app/bootstrap.php';

// Authentification requise
if (!Auth::logged()) redirect('index.php');

$offre = new OffreEmbauche($_GET['offre']);
$listeDocs = $offre->tousSesDocs();

//si on est candidat on peut voir une offre et y postuler
if (Auth::user()->estCandidat()) {

    $layout = new Layout('candidats');
    include view('candidats/offre.php');
    $layout->show($offre->poste);
}

//si on est responsable on peut voir l'offre
if(Auth::user()->estResponsable()) {

    $layout = new Layout('responsables');
    include view('responsables/offre.php');
    $layout->show($offre->poste);
}
	

?>