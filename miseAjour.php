<?php

require 'app/bootstrap.php';
//Authentification requise
if (!Auth::logged()) redirect('index.php');

//trouve le nom du fichier et vérifie qu'il existe
$basename = basename($_FILES['uploadedfile']['name']);
if(!$basename)
{
    flash("Veuillez sélectionner un ficher");
    redirect('documents.php');
}

//trouve l'extension
$extension = pathinfo($basename, PATHINFO_EXTENSION);

//le nom du chemin
$target_path = __DIR__ . "/documents/";
$emplacement = Auth::user()->nom . sha1($basename . time());
$target_path = $target_path . $emplacement;

$target_path = $target_path . "." . $extension;


//supprime l'ancience document
$doc = new Document($_POST['numAncienFichier']);

if (is_file($target_path)) (unlink($doc->chemin));

if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
    echo "Le fichier " . basename($_FILES['uploadedfile']['name']) . " a bien été mis à jour. ";

    //met à jour l'objet concerné avec les nouvelles données
    $doc->updateDocument(basename($_FILES['uploadedfile']['name']), $target_path, $emplacement);

    //redirige vers documents.php
    redirect('documents.php');
} else echo "Erreur avec la mise à jour du fichier. Le fichier est probablement tros gros.";


