<?php

require 'app/bootstrap.php';

//Authentification requise
if (!Auth::logged()) redirect('index.php');

//si responsable on peut voir les offres d'embauche
if(Auth::user()->estResponsable())
{
    $layout = new Layout('responsables');
    include view('responsables/ajouterOffre.php');
    $layout->show('Ajouter une offre d\'embauche');
} else redirect("index.php");