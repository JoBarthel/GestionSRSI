<?php

require 'app/bootstrap.php';

// Authentification requise
if(!Auth::logged()) redirect('index.php');

//affiche la liste des responsables et leurs adresses mail
if(Auth::user()->estCandidat()){

    $listeResponsable = Responsable::listerResponsables();

	$layout = new Layout('candidats');
	include view('candidats/contact.php');
	$layout->show('Contacts');
}
else
{
    $listeResponsable = Responsable::listerResponsables();

    $layout = new Layout('responsables');
    include view('responsables/contacts.php');
    $layout->show('Contacts');
}