<?php

require 'app/bootstrap.php';

// Si $_POST['username'] et $_POST['password'] sont absents ou vides, on redirige vers la page de connexion
if(!sent('username', 'password')) {
    flash("Veuillez remplir le formulaire !");
    redirect('index.php');
}

$user = filter(input('username'));
$password = filter(input('password'));

if($id = Utilisateur::verifier($user, $password))
{
    Auth::login($id);
}
else {
    // On affiche un message d'erreur sur la page du formulaire
    flash('Mauvais identifiants !');
}

redirect('index.php');