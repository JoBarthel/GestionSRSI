<?php
/**
 * Created by PhpStorm.
 * User: Célia
 * Date: 27/12/2016
 * Time: 17:39
 */

require 'app/bootstrap.php';

if (!Auth::logged() || !Auth::user()->estCandidat()) redirect('index.php');

$user = Auth::user();
$offre=$_GET['numOffre'];
$numEnt=$_GET['numEntretien'];
$ent = new Entretien($numEnt);
$ent->supprimeEntretien();
redirect('ficheEnt.php?offre='.$offre);
?>