<?php

require('app/bootstrap.php');

// Authentification requise
if (!Auth::logged()) redirect('index.php');

//être un responsable requis
if (!Auth::user()->estResponsable()) {
    redirect('index.php');
}

$listeCandidat = Candidat::listerCandidats();
$candidatsValide = [];
$candidatsRefuses = [];

$layout = new Layout('responsables');
include view('responsables/listeCandidats.php');
$layout->show('Informations');
