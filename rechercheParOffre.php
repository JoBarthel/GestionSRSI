<?php
require 'app/bootstrap.php';

//authentification requise
if (!Auth::logged()) redirect('index.php');

//être un responsable requis
if(!Auth::user()->estResponsable()) redirect('index.php');

//liste déroulante donc pas besoin de valider les inputs
$offre = $_POST['offre'];
$liste = Candidat::rechercheParOffre($offre);

//envoie vers la vue
$layout = new Layout('responsables');
include view('responsables/rechercheParOffre.php');
$layout->show('Recherche');