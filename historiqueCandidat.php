<?php

require 'app/bootstrap.php';

// Authentification requise
if(!Auth::logged()) redirect('index.php');

if (Auth::user()->estResponsable()) {


    $candidat = new Candidat($_GET['numCandidat']);

    $layout = new Layout('responsables');
    include view('responsables/historiqueCandidat.php');
    $layout->show('Historique');

} else redirect('index.php');