<?php
require 'app/bootstrap.php';

//authentification requise
if (!Auth::logged()) redirect('index.php');

//être un responsable requis
if(!Auth::user()->estResponsable()) redirect('index.php');

//liste déroulante donc pas besoin de valider les inputs
$statut = $_POST['statut'];
$liste = Candidat::rechercheParStatut($statut);

//envoie vers la vue
$layout = new Layout('responsables');
include view('responsables/rechercheParStatut.php');
$layout->show('Recherche');