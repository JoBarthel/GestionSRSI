<?php

require 'app/bootstrap.php';

// Authentification requise
if(!Auth::logged()) redirect('index.php');
if(!Auth::user()->estCandidat()) error(403);

$user = Auth::user();

$layout = new Layout('candidats');
include view('candidats/informations.php');
$layout->show('Informations');