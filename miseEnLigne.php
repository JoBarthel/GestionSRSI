<?php

require 'app/bootstrap.php';
//Authentification requise
if (!Auth::logged()) redirect('index.php');

//trouve le nom du fichier et vérifie qu'il n'est pas vide
$basename = basename($_FILES['uploadedfile']['name']);
if(!$basename)
{
    flash("Veuillez sélectionner un ficher");
    redirect('documents.php');
}

//trouve l'extension
$extension = pathinfo($basename, PATHINFO_EXTENSION);

//le nom du chemin
$target_path = __DIR__ . "/documents/";
$emplacement = Auth::user()->nom . sha1($basename . time());
$target_path = $target_path . $emplacement;

$target_path = $target_path . "." . $extension;

if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
    echo "Le fichier " . basename($_FILES['uploadedfile']['name']) . " a bien été mis en ligne.";

    //crée un nouvel objet document avec cet objet
    Document::ajouteDocument(basename($_FILES['uploadedfile']['name']), Auth::user()->numUtilisateur, $_POST["type"], $target_path, 0, $emplacement);

    //redirige vers documents.php
    redirect('documents.php');

} else echo "Erreur avec la mise en ligne du fichier. Le fichier est probablement trop gros.    ";
