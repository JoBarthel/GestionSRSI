<?php

require 'app/bootstrap.php';

// Authentification requise
if(!Auth::logged()) redirect('index.php');

if (Auth::user()->estResponsable()) {

    $utilisateur = Utilisateur::get(Auth::user()->numUtilisateur);

    $layout = new Layout('responsables');
    include view('responsables/configurerAlertesMail.php');
    $layout->show('Alertes mail');

} else redirect('index.php');