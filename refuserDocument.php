<?php

require('app/bootstrap.php');

// Authentification requise
if (!Auth::logged()) redirect('index.php');

//être un responsable requis
if (!Auth::user()->estResponsable()) {
    redirect('index.php');
}

$numDoc = $_GET['numDoc'];
$numCandidat = $_GET['numCandidat'];
$listeRaisonRefus = RaisonRefus::lister();

$layout = new Layout('responsables');
include view('responsables/refuserDocument.php');
$layout->show('Refuser un document');
