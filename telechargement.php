<?php
require 'app/bootstrap.php';

// Authentification requise
if(!Auth::logged()) redirect('index.php');

$file = __DIR__."\documents\\".basename($_GET['path']);

if(!is_file($file)) { echo "Erreur sur : ".$file; }
else {

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . $file . '"');
    header('Content-Transfer-Encoding: binary');
    header('Connection: Keep-Alive');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');

	readfile($file);

}

?>